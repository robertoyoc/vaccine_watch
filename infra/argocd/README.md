# Install argo
- kubectl apply -f argocd/namespace.yaml 
- kubectl apply -n argocd -f argocd/argo.yaml 
- kubectl apply -f argocd/ingress.yaml 
- Create a new project called `argocd` in rancher manager
- Move `argocd` namespace to `argocd` project
- Use globalDNS to point the domain you used in ingress to the created proyect
- Retrieve admin password using `kubectl get pods -n argocd -l app.kubernetes.io/name=argocd-server -o name | cut -d'/' -f 2`
- Login to using your browser with user `admin` and the password you got from the previous step.