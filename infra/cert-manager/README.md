# Install cert-manager
- kubectl apply -f cert-manager/namespace.yaml
- helm repo add jetstack https://charts.jetstack.io
- helm repo update
- helm install cert-manager jetstack/cert-manager --namespace cert-manager --version v1.3.0 --set installCRDs=true
- kubectl apply -f cert-manager/letsencrypt-prod.yaml