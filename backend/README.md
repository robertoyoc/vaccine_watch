# Backend documentation

Backend for Mongo connection.

## Installation

Use npm install to install required dependencies.

```bash
npm install 
```

## Usage
If running locally, start mongod in a separate tab.
```bash
mongod
```
Then run 
```bash
node server.js
```



Otherwise, change db/mongoose db url to Mongo Atlas endpoint

# User endpoints
## localhost:3000/users/createUser <br>
To create user, pass the following fields inside of the body request. <br>
x-www-form-urlencoded <br>

Fields :

    - name (STR)
    - secondName (STR)
    - age (NUM)
    - sex (STR)
    - curp (STR)
    - email (STR)
    - phone (NUM)
    - position (STR)
    - password (STR)
    - uuid (STR) (cognito autogen key)

If you are creating an admin user the following fields are also needed as specified:

Fields :

    - doctor_id (STR)
    - currentCtr (STR)
    - position (STR) (Must be either "Médico administrador de vacunas" or "Supervisor de vacunación")

The first represents the id of the doctor, that must be unique, and the second represents the id of the vaccination center.

Update: If there are vaccination centers in the borough, the user will automatically be assigned the closest to him. <br>
Each new user registered, updates the priority queue and all the users´ priorities automatically.<br>
The request will return "success": true if user was inserted succesfully; false otherwise

## localhost:3000/users/getUsers <br>
Returns a list of all users.
The request will return "success": true if sucessful alongside a "data" array containing all existing users in the database; false otherwise

Example:
```json
{
    "success": true,
    "data": [
        {
            "_id": "604aa387fe118870ec7a89c0",
            "name": "Ed",
            "secondName": "Bad",
            "age": 22,
            "sex": "H",
            "curp": "doiagfhjadufoiu",
            "email": "edvs@eds.com",
            "phone": 165406845,
            "position": "tech",
            "password": "$2b$08$rPPZi.y31tvelbHSAOJxBuiaVk6CeS8KeHOgQTJN/jesTMW4DAsGy",
            "__v": 0
        },
        {
            "_id": "604aa4b7fe118870ec7a89c1",
            "name": "Ed2",
            "secondName": "Bad",
            "age": 22,
            "sex": "H",
            "curp": "doiagfhjadufoiu",
            "email": "edvs@eds.com",
            "phone": 165406845,
            "position": "tech",
            "password": "$2b$08$rnjm.VosRgv8zlrmlM.98OGNxAITn03.dho0CMDvMlUuUHanUEdum",
            "__v": 0
        }
    ]
}
```
## localhost:3000/users/getUsers/:borough
Retrieve a list of all users registered in the borough with as the "data" array. True if successful, false otherwise.

## localhost:3000/users/getUser/:uuid <br>
Pass as parameter the user´s uuid (The cognito autogen id). <br>
The request will return solely the user object. <br> 500 error if user was not found.

## localhost:3000/users/usersAssignation/:borough
Updates manually the list of priorities and each user's priority in the given borough. <br>
Returns true if successful, false otherwise.

## localhost:3000/users/updateUser/:uuid <br>
Pass as parameter the user´s cognito id, <br> Pass in body of request fields to change <br>
The request will return Success: true if sucessful false otherwise; and it will also return the updated user.

## localhost:3000/users/deleteUser/:uuid <br>
Pass as parameter the user´s cognito id
Success: true if sucessful false otherwise. Will return the deleted user alongside the success message. 

## localhost:3000/users/login <br>
To login user, pass the following fields inside of the body request. <br>
x-www-form-urlencoded <br>

Fields :

    - email (STR)
    - password (STR)

    
The request will return "success": true if user was found and password matches. It will also return the user object as the "data" field. If credentials doesn't match it will return "success": false and an error message. 

## localhost:3000/users/userConfirmation/:uuid
The endpoint changes the user's userApproved flag to true, stating that it confirms the vaccination has taken place.<br>
Returns true if successful, false otherwise.

## localhost:3000/users/getConfirmedUsers/:phase_id
The endpoint returns success:true plus a list of all users that have confirmed its assistance to that vaccination phase. 

## localhost:3000/users/confirmAttendance/:uuid/:response
The endpoint updates the hasAccepted flag to 1 so as to confirm the user has accepted his vaccination date and is confirming his attendance. <br>
If the user doesn't confirm, it will erase all user data pertaining to the vaccination and update the vaccination phase accordingly. <br>
The user will then be a candidate again for future vaccination phases in that borough. User cannot confirm its attendance the day of the vaccination <br> If the user doesn't pass any of the checks, the confirmation will fail and no change will take place; the endpoint will return a success:false. <br>

# Borough  endpoints

## localhost:3000/bors/createBorough <br>
To create borough, pass the following fields inside of the body request. <br>
x-www-form-urlencoded <br>

Fields :

    - name (STR)
    - vaccines (NUM)

## localhost:3000/bors/getBors <br>
Returns a list of all users.
The request will return "success": true if sucessful alongside a "data" array containing all existing boroughs in the database; false otherwise


# Occupation  endpoints

## localhost:3000/occs/createOccupation <br>
To create new occupation, pass the following fields inside of the body request. <br>
x-www-form-urlencoded <br>

Fields :

    - name (STR)

## localhost:3000/occs/getOccupations <br>
Returns a list of all occupations.
Returns names of all existing occupations inside a "data" array; false otherwise

# Admin endpoints

## localhost:3000/admin/getUserByCurp/:curp <br>
Returns a user by curp, returns false if no user is found. 

## localhost:3000/admin/checkUserAttendance/:curp
Returns success:true plus a message if the user has confirmed its assistance, <br>
returns success: false otherwise. 

## localhost:3000/admin/requestUser/:curp/:doctor_id
Change the isRequested user tag to true if the user with the given curp exists. This implies that an admin user is requesting certification for the given patient. The parameters require the curp of the user to request and the id of the doctor that is requesting. Returns true if the change is successful, false if the user doesn't exist or if it is already requested or if the doctor cannot perform this operation.

## localhost:3000/admin/getRequestingUsers/:assignedCtr
Retrieves all users of a given vaccination center whose tags userApproved and isRequested are true along with a success:true. Returns success:false if no users were found. This endpoint retrieves all users in a borough that have both confirmed they've been vaccinated and that an admin is requesting certification for it. 

## localhost:3000/admin/confirmVaccination/:uuid/:doctor_id
Confirms that a user has been vaccinated and changes the user's isVaccinated tag to true and returns a success:true message. The parameters require the uuid of the user to be confirmed and the id of the doctor that is confirming it. Returns sucess:false if the patient hasn't confirmed its vaccination or if an admin hasn't requested a certification for that patient or if that patient has already been confirmed as vaccinated or if that type of doctor cannot perform this operation. 

## localhost:3000/admin/getUsers/:doctor_id
Gets all users that were confirmed or requested by the specified doctor, found by its id. 

## localhost:3000/admin/getUsers/
Gets all users that are not doctors. 


# Center endpoints

## localhost:3000/centers/createCenter
To create new center, pass the following fields inside of the body request. <br>
x-www-form-urlencoded <br>

Fields :

    - name (STR) (required)
    - boroughName (STR) (required)
    - lat (NUM) (required)
    - lon (NUM) (required)
    - address (STR)

The newly created center will automatically add existing users to it, if the distance is shorter or if the user(s) <br>
have no previously assigned vaccination center. <br>
The endpoint will automatically create an ID for the center composed as borougnName_name. It will return <br>
success:true if the center is created successfully, success:false otherwise. 

## localhost:3000/centers/getCenter/:id_center
Retrieve a particular center, returned as a "ctr" object. Pass as parameter the id of the desired center. <br>

## localhost:3000/centers/getCenters/:bName
Returns all of the vaccination centers in the given borough as a list named "data". Pass as parameters the name of the borough. <br>


## localhost:3000/centers/assignLoc/:id_center/:lat/:lon
Pass as parameters the id of the center to be updated, and the lat and lon coordinates to assign. <br>
Returns success:true if change was successful, success:false otherwise. 

