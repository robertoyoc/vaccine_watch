const express = require('express');
const cors = require('cors');
const bodyparser = require('body-parser');

// call db
require('./db/mongoose');

const userRouter = require('./routes/user.routes');
const boroRouter = require('./routes/borough.routes');
const occRouter = require('./routes/occupation.routes');
const centerRouter = require('./routes/center.routes');
const adminRouter = require('./routes/admin.routes');
const certRouter = require('./routes/cert.routes');
const app = express();
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({
    extended: true
}));
app.use(cors());
//app.use(express.json())

app.use('/users', userRouter);
app.use('/bors', boroRouter);
app.use('/occs', occRouter);
app.use('/admin', adminRouter);
app.use('/centers', centerRouter);
app.use('/certificate', certRouter);

app.get('/healthz', (req, res) => {
    res.status(200).send('OK')
})

const port = process.env.PORT || 3000;

app.listen(port, () => {
    console.log('Server is up on port ' + port)
})