const mongoose = require('mongoose')



const server = process.env.MONGO_SERVER || "mongodb://127.0.0.1:27017";

mongoose.connect(`${server}/vaccineWatch`, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true
}).then(() => {
    console.log('Database connected sucessfully.')
},
    error => {
        console.log('Database could not be connected : ' + error)
    }
)