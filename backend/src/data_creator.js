
function sortNumber(a,b) {
    return a - b;
}

function quantile(array, percentile) {
    array.sort(sortNumber);
    index = percentile/100. * (array.length-1);
    if (Math.floor(index) == index) {
    	result = array[index];
    } else {
        i = Math.floor(index)
        fraction = index - i;
        result = array[i] + (array[i+1] - array[i]) * fraction;
    }
    return result;
}

function quantile_on_obj(array, percentile) {
    index = percentile/100. * (array.length-1);
    if (Math.floor(index) == index) {
    	result = array[index];
    } else {
        i = Math.floor(index)
        fraction = index - i;
        result = array[i] + (array[i+1] - array[i]) * fraction;
    }
    return result;
}

// Transform ages with occupation data to produce a new factor for each user
// These are the default weights if no other are given.
let occs = new Object();
// Weights
occs['Médico'] = 1.5;
occs['Docente'] = 1.25;
occs['Público'] = 1.2;
occs['Comida/Construcción'] = 1.15;
occs['otros'] = 1.00;
occs['Estudiante'] = 1.1;
// Admin Weights
occs['Médico administrador de vacunas'] = occs['Médico'];
occs['Supervisor de vacunación'] = occs['Médico'];

// Given an array of objects, return priority and "t" factor for all elements
// Returns modified array of objects with these new fields: priority, t, schedule
function real(data, occupations, nPriorityLvls = 10){

    // Get data length
    const n = data.length;
    
    console.log("Number of registries found: ", n, "\n");
    
    if(n<10){ // Deal with cases with few registries
        console.log("Current population is small, changing priority levels to compensate...\n");
        nPriorityLvls = n;
    }

    // verify if there are occupations 
    let occObj = new Object();
    if(occupations.length>0){
        console.log("Using found weights");
        occupations.forEach(x=>{
            occObj[x.name] = x.priority;
        });
    } else {
        console.log("Using default weights");
        // if none are found, use the default weights
        occObj = occs;
    }
    let ages = [];
    // Execute the transformations
    data.forEach(element => {
        // Calculate transformation TODO: port function
        
        if(occObj[element.position]!==undefined){
            element.t = element.age * occObj[element.position];
        } else {
            element.t = element.age;
        } 
        
        ages.push(element.age);
        
        // Print
        //console.log("Name: ", element.name," Age: ", element.age," Occupation: ", element.position," factor: ", element.t);
    });

    // sort on t
    data.sort((a, b) => (a.t > b.t) ? 1 : (a.t === b.t) ? ((a.t > b.t) ? 1 : -1) : -1 )
    
    // Print the sorted array on t
    //console.log("\nSorted array based on t:\n");

    // Store each t factor element into a separate array to perform the percentile calculation
    // we don't have to perform sorting each time for the data array of objects is sorted already
    let arrT = [];
    for(let i=0; i<n; i++){
        arrT.push(data[i].t); 
        //console.log("Name: ", data[i].name," Age: ", data[i].age," Occupation: ", data[i].position," factor: ", data[i].t, " above q:", data[i].q);
    }
       
    // get the percentiles of the data according to the number of priority levels desired
    let factor = 100;
    deciles = []
    let a = factor/(nPriorityLvls)
    for(let i=a; i<factor+1; i=i+a){
        //console.log(i);
        deciles.push(i);
    }

    // Get the boundaries for each decile or percentile
    let qArray = [];
    qArray.push(new Object());
    qArray[0].start = quantile_on_obj(arrT, 100-deciles[0]);
    qArray[0].end = 200;
    
    for(let i=1; i<deciles.length; i++){
        qArray[i] = new Object();
        qArray[i].end = qArray[i-1].start;
        qArray[i].start = Math.floor(deciles[i])!=100? quantile_on_obj(arrT, 100-deciles[i]):quantile_on_obj(arrT, 0)
       }
    
    // Print the boundaries for each decile
    /* qArray.forEach(e=>{
        console.log("end", e.end);
        console.log("start", e.start);
    }) */
    
    // Iterate through each decile boundary to determine where datapoint 'p' belongs to
    let prior = (p) => {
        let prLevel = 1;
        for(let i=0; i<qArray.length; i++){
            if(p>=qArray[i].start && p<qArray[i].end){
                break;
            }
            prLevel++;
        }
        return prLevel;
    }
    
    // Iterate through all elements to assign to each a priority with t factors
    data.forEach(e=>{
        e.priority = prior(e.t);
    })

    /* data.forEach(e=>console.log(" Age: ", e.age, " Priority: ", e.priority)) */

    let training_data = [];
    /* data.forEach(e=>{
        
        let oneHot;
        switch(e.position){
            case 'Comida/Construcción':
                oneHot = [1,0,0,0,0];
                break;
            case 'Público':
                oneHot = [0,1,0,0,0];
                break;
            case 'Docente':
                oneHot = [0,0,1,0,0];
                break;
            case 'Médico':
                oneHot = [0,0,0,1,0];
                break;
            case 'otros':
                oneHot = [0,0,0,0,1];
                break;
            default:
                oneHot = [0,0,0,0,1];
        }

        oneHot.push(e.age);
        console.log("{ input: [" + oneHot.toString() +"], output: { " + e.priority + ":1}},")
    }); */
    let min = Math.min(...ages);
    let max = Math.max(...ages);
    console.log(min)
    console.log(max)
    data.forEach(e=>{
        let oneHot;
        /* switch(e.position){
            case 'Comida/Construcción':
                oneHot = 4;
                break;
            case 'Público':
                oneHot = 6;
                break;
            case 'Docente':
                oneHot = 8;
                break;
            case 'Médico':
                oneHot = 10;
                break;
            case 'otros':
                oneHot = 2;
                break;
            case 'Estudiante':
                oneHot = 2;
                break; */
        switch(e.position){
            case 'Médico':
                oneHot = 10;
                break;
            case 'Docente':
                oneHot = 7;
                break;
            case 'Público':
                oneHot = 6;
                break;
            case 'Comida/Construcción':
                oneHot = 5;
                break;
            case 'Estudiante':
                oneHot = 3;
                break;
            case 'otros':
                oneHot = 2;
                break;  
            default:                
                oneHot = 2;
        }

        let age_t = (e.age - min)/(max-min);
        /* console.log("{input:{ \"position\": \""+ e.position + "\", \"age\":\""+ e.age + "\"}, output: {\"priority\": \"" + e.priority + "\"}},"); */
        let py = (e.priority - 1)/(10-1);
        let pos = (oneHot - 2)/(10-2);
        //console.log("{input:{ position: "+ oneHot + ", age:"+ age_t + "}, output: { " + py + ":1}},");
        //console.log("{input:{ position: "+ pos + ", age:"+ age_t + "}, output: [" + py + "]},"); // i like this one
        console.log("{input:{ position: "+ pos + ", age:"+ age_t + "}, output: { " + e.priority + ":1}},"); 
    });

    return training_data;
}

dummy_data = [
    {'position': 'Comida/Construcción', 'age': 29.0}, {'position': 'Estudiante', 'age': 21.0}, {'position': 'Comida/Construcción', 'age': 27.0}, {'position': 'otros', 'age': 19.0}, {'position': 'Estudiante', 'age': 18.0}, {'position': 'Público', 'age': 23.0}, {'position': 'Comida/Construcción', 'age': 22.0}, {'position': 'Estudiante', 'age': 23.0}, {'position': 'Público', 'age': 31.0}, {'position': 'otros', 'age': 25.0}, {'position': 'Público', 'age': 21.0}, {'position': 'Público', 'age': 21.0}, {'position': 'Público', 'age': 24.0}, {'position': 'otros', 'age': 24.0}, {'position': 'Público', 'age': 19.0}, {'position': 'otros', 'age': 22.0}, {'position': 'Estudiante', 'age': 31.0}, {'position': 'otros', 'age': 28.0}, {'position': 'Público', 'age': 33.0}, {'position': 'otros', 'age': 27.0}, {'position': 'Estudiante', 'age': 24.0}, {'position': 'otros', 'age': 22.0}, {'position': 'Comida/Construcción', 'age': 31.0}, {'position': 'Estudiante', 'age': 29.0}, {'position': 'Comida/Construcción', 'age': 31.0}, {'position': 'Estudiante', 'age': 21.0}, {'position': 'Comida/Construcción', 'age': 22.0}, {'position': 'Comida/Construcción', 'age': 28.0}, {'position': 'Público', 'age': 22.0}, {'position': 'Comida/Construcción', 'age': 23.0}, {'position': 'otros', 'age': 23.0}, {'position': 'otros', 'age': 20.0}, {'position': 'Público', 'age': 38.0}, {'position': 'Público', 'age': 25.0}, {'position': 'Comida/Construcción', 'age': 21.0}, {'position': 'Estudiante', 'age': 37.0}, {'position': 'Estudiante', 'age': 27.0}, {'position': 'Público', 'age': 20.0}, {'position': 'Público', 'age': 21.0}, {'position': 'Comida/Construcción', 'age': 19.0}, {'position': 'Comida/Construcción', 'age': 21.0}, {'position': 'Público', 'age': 28.0}, {'position': 'otros', 'age': 27.0},
    {'position': 'Estudiante', 'age': 26.0}, {'position': 'Público', 'age': 24.0}, {'position': 'Estudiante', 'age': 31.0}, {'position': 'Estudiante', 'age': 20.0}, {'position': 'Público', 'age': 19.0}, {'position': 'Comida/Construcción', 'age': 26.0}, {'position': 'Público', 'age': 23.0}, {'position': 'otros', 'age': 31.0}, {'position': 'Público', 'age': 21.0}, {'position': 'Estudiante', 'age': 29.0}, {'position': 'Comida/Construcción', 'age': 27.0}, {'position': 'Estudiante', 'age': 22.0}, {'position': 'otros', 'age': 36.0}, {'position': 'Comida/Construcción', 'age': 19.0}, {'position': 'otros', 'age': 24.0}, {'position': 'Estudiante', 'age': 19.0}, {'position': 'Estudiante', 'age': 25.0}, {'position': 'Público', 'age': 19.0}, {'position': 'Comida/Construcción', 'age': 21.0}, {'position': 'Público', 'age': 22.0}, {'position': 'Comida/Construcción', 'age': 26.0}, {'position': 'Estudiante', 'age': 25.0}, {'position': 'otros', 'age': 18.0}, {'position': 'Público', 'age': 25.0}, {'position': 'otros', 'age': 25.0}, {'position': 'Comida/Construcción', 'age': 29.0}, {'position': 'Público', 'age': 27.0}, {'position': 'Comida/Construcción', 'age': 33.0}, {'position': 'Comida/Construcción', 'age': 31.0}, {'position': 'otros', 'age': 20.0}, {'position': 'Comida/Construcción', 'age': 31.0}, {'position': 'Estudiante', 'age': 22.0}, {'position': 'Comida/Construcción', 'age': 21.0}, {'position': 'Comida/Construcción', 'age': 23.0}, {'position': 'otros', 'age': 19.0}, {'position': 'Comida/Construcción', 'age': 24.0}, {'position': 'otros', 'age': 21.0}, {'position': 'otros', 'age': 23.0}, {'position': 'Público', 'age': 24.0}, {'position': 'Comida/Construcción', 'age': 22.0}, {'position': 'Comida/Construcción', 'age': 20.0}, {'position': 'otros', 'age': 20.0}, {'position': 'otros', 'age': 20.0}, {'position': 'Estudiante', 'age': 23.0}, {'position': 'otros', 'age': 20.0}, {'position': 'otros', 'age': 23.0}, {'position': 'Comida/Construcción', 'age': 24.0},
    {'position': 'otros', 'age': 41.0}, {'position': 'Docente', 'age': 45.0}, {'position': 'Público', 'age': 43.0}, {'position': 'Médico', 'age': 37.0}, {'position': 'Docente', 'age': 45.0}, {'position': 'Público', 'age': 30.0}, {'position': 'Comida/Construcción', 'age': 37.0}, {'position': 'Comida/Construcción', 'age': 48.0}, {'position': 'Médico', 'age': 39.0}, {'position': 'Comida/Construcción', 'age': 47.0}, {'position': 'Público', 'age': 50.0}, {'position': 'Público', 'age': 47.0}, {'position': 'Médico', 'age': 44.0}, {'position': 'otros', 'age': 46.0}, {'position': 'Docente', 'age': 49.0}, {'position': 'otros', 'age': 49.0}, {'position': 'Comida/Construcción', 'age': 42.0}, {'position': 'otros', 'age': 49.0}, {'position': 'Comida/Construcción', 'age': 40.0}, {'position': 'Público', 'age': 38.0}, {'position': 'Médico', 'age': 51.0}, {'position': 'otros', 'age': 43.0}, {'position': 'Comida/Construcción', 'age': 44.0}, {'position': 'Docente', 'age': 40.0}, {'position': 'Docente', 'age': 45.0}, {'position': 'otros', 'age': 44.0}, {'position': 'Público', 'age': 46.0}, {'position': 'otros', 'age': 46.0}, {'position': 'Médico', 'age': 46.0}, {'position': 'otros', 'age': 47.0}, {'position': 'Público', 'age': 49.0}, {'position': 'Docente', 'age': 42.0}, {'position': 'Público', 'age': 47.0}, {'position': 'Médico', 'age': 48.0}, {'position': 'Público', 'age': 43.0}, {'position': 'otros', 'age': 30.0}, {'position': 'otros', 'age': 37.0}, {'position': 'Público', 'age': 39.0}, {'position': 'Público', 'age': 45.0}, {'position': 'Público', 'age': 39.0}, {'position': 'Docente', 'age': 41.0}, {'position': 'otros', 'age': 45.0}, {'position': 'Comida/Construcción', 'age': 41.0}, {'position': 'Médico', 'age': 44.0}, {'position': 'Docente', 'age': 44.0}, {'position': 'otros', 'age': 51.0}, {'position': 'Público', 'age': 47.0}, {'position': 'Público', 'age': 43.0}, {'position': 'Comida/Construcción', 'age': 50.0}, {'position': 'Médico', 'age': 45.0},
    {'position': 'Público', 'age': 51.0}, {'position': 'Público', 'age': 41.0}, {'position': 'Docente', 'age': 44.0}, {'position': 'otros', 'age': 41.0}, {'position': 'Comida/Construcción', 'age': 48.0}, {'position': 'Público', 'age': 45.0}, {'position': 'Público', 'age': 50.0}, {'position': 'Médico', 'age': 39.0}, {'position': 'Público', 'age': 41.0}, {'position': 'Público', 'age': 49.0}, {'position': 'Público', 'age': 52.0}, {'position': 'Médico', 'age': 53.0}, {'position': 'Comida/Construcción', 'age': 37.0}, {'position': 'Médico', 'age': 48.0}, {'position': 'otros', 'age': 40.0}, {'position': 'Médico', 'age': 41.0}, {'position': 'Comida/Construcción', 'age': 41.0}, {'position': 'Comida/Construcción', 'age': 39.0}, {'position': 'Público', 'age': 39.0}, {'position': 'Docente', 'age': 42.0}, {'position': 'Comida/Construcción', 'age': 41.0}, {'position': 'otros', 'age': 51.0}, {'position': 'Médico', 'age': 44.0}, {'position': 'otros', 'age': 44.0}, {'position': 'Médico', 'age': 39.0}, {'position': 'Comida/Construcción', 'age': 35.0}, {'position': 'otros', 'age': 45.0}, {'position': 'Comida/Construcción', 'age': 39.0}, {'position': 'Comida/Construcción', 'age': 47.0}, {'position': 'Docente', 'age': 37.0}, {'position': 'Público', 'age': 42.0}, {'position': 'Médico', 'age': 40.0}, {'position': 'Docente', 'age': 46.0}, {'position': 'Médico', 'age': 44.0}, {'position': 'Docente', 'age': 48.0}, {'position': 'otros', 'age': 44.0}, {'position': 'Comida/Construcción', 'age': 49.0}, {'position': 'Comida/Construcción', 'age': 46.0}, {'position': 'Médico', 'age': 51.0}, {'position': 'Público', 'age': 40.0}, {'position': 'Comida/Construcción', 'age': 45.0}, {'position': 'Público', 'age': 46.0}, {'position': 'otros', 'age': 43.0}, {'position': 'Docente', 'age': 44.0}, {'position': 'Público', 'age': 50.0}, {'position': 'Público', 'age': 38.0}, {'position': 'Médico', 'age': 43.0}, {'position': 'Médico', 'age': 40.0}, {'position': 'Docente', 'age': 51.0}, {'position': 'otros', 'age': 45.0},
    {'position': 'Docente', 'age': 79.0}, {'position': 'Médico', 'age': 64.0}, {'position': 'Docente', 'age': 68.0}, {'position': 'Público', 'age': 63.0}, {'position': 'otros', 'age': 71.0}, {'position': 'otros', 'age': 64.0}, {'position': 'Médico', 'age': 72.0}, {'position': 'otros', 'age': 64.0}, {'position': 'Docente', 'age': 62.0}, {'position': 'Médico', 'age': 61.0}, {'position': 'Médico', 'age': 66.0}, {'position': 'Médico', 'age': 68.0}, {'position': 'Comida/Construcción', 'age': 71.0}, {'position': 'Médico', 'age': 61.0}, {'position': 'Docente', 'age': 68.0}, {'position': 'otros', 'age': 66.0}, {'position': 'Público', 'age': 66.0}, {'position': 'Médico', 'age': 69.0}, {'position': 'Comida/Construcción', 'age': 69.0}, {'position': 'Público', 'age': 68.0}, {'position': 'Docente', 'age': 61.0}, {'position': 'Médico', 'age': 68.0}, {'position': 'Público', 'age': 70.0}, {'position': 'Médico', 'age': 58.0}, {'position': 'Público', 'age': 61.0}, {'position': 'Público', 'age': 65.0}, {'position': 'Público', 'age': 69.0}, {'position': 'Docente', 'age': 66.0}, {'position': 'Público', 'age': 69.0}, {'position': 'Público', 'age': 63.0}, {'position': 'Comida/Construcción', 'age': 65.0}, {'position': 'Público', 'age': 67.0}, {'position': 'Comida/Construcción', 'age': 71.0}, {'position': 'Médico', 'age': 66.0}, {'position': 'Comida/Construcción', 'age': 62.0}, {'position': 'Médico', 'age': 67.0}, {'position': 'Público', 'age': 61.0}, {'position': 'otros', 'age': 65.0}, {'position': 'Médico', 'age': 76.0}, {'position': 'Médico', 'age': 64.0}, {'position': 'otros', 'age': 65.0}, {'position': 'Público', 'age': 55.0}, {'position': 'otros', 'age': 68.0}, {'position': 'Comida/Construcción', 'age': 66.0}, {'position': 'Médico', 'age': 65.0}, {'position': 'Médico', 'age': 72.0}, {'position': 'otros', 'age': 72.0}, {'position': 'Comida/Construcción', 'age': 65.0}, {'position': 'Médico', 'age': 65.0},
    {'position': 'otros', 'age': 67.0}, {'position': 'Médico', 'age': 73.0}, {'position': 'Comida/Construcción', 'age': 71.0}, {'position': 'otros', 'age': 58.0}, {'position': 'Docente', 'age': 69.0}, {'position': 'Médico', 'age': 76.0}, {'position': 'Público', 'age': 73.0}, {'position': 'Comida/Construcción', 'age': 59.0}, {'position': 'Comida/Construcción', 'age': 63.0}, {'position': 'Docente', 'age': 62.0}, {'position': 'Docente', 'age': 61.0}, {'position': 'otros', 'age': 60.0}, {'position': 'Comida/Construcción', 'age': 62.0}, {'position': 'Público', 'age': 62.0}, {'position': 'Comida/Construcción', 'age': 67.0}, {'position': 'Médico', 'age': 62.0}, {'position': 'Docente', 'age': 61.0}, {'position': 'Comida/Construcción', 'age': 66.0}, {'position': 'Médico', 'age': 67.0}, {'position': 'otros', 'age': 62.0}, {'position': 'Médico', 'age': 64.0}, {'position': 'Médico', 'age': 60.0}, {'position': 'Público', 'age': 64.0}, {'position': 'Docente', 'age': 63.0}, {'position': 'otros', 'age': 73.0}, {'position': 'Médico', 'age': 67.0}, {'position': 'Docente', 'age': 80.0}, {'position': 'Médico', 'age': 64.0}, {'position': 'Público', 'age': 58.0}, {'position': 'Médico', 'age': 55.0}, {'position': 'Comida/Construcción', 'age': 64.0}, {'position': 'Comida/Construcción', 'age': 55.0}, {'position': 'otros', 'age': 68.0}, {'position': 'Público', 'age': 64.0}, {'position': 'Médico', 'age': 67.0}, {'position': 'Comida/Construcción', 'age': 70.0}, {'position': 'Docente', 'age': 66.0}, {'position': 'otros', 'age': 62.0}, {'position': 'Médico', 'age': 59.0}, {'position': 'Público', 'age': 54.0}, {'position': 'Docente', 'age': 63.0}, {'position': 'Docente', 'age': 64.0}, {'position': 'Comida/Construcción', 'age': 60.0}, {'position': 'Comida/Construcción', 'age': 69.0}, {'position': 'Docente', 'age': 66.0}, {'position': 'Docente', 'age': 56.0}, {'position': 'Comida/Construcción', 'age': 64.0}, {'position': 'Público', 'age': 56.0}, {'position': 'Médico', 'age': 66.0}
]

let results = real(dummy_data, []);


