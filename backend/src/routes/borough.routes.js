const express = require('express');
const {createDate, real_priority, cut_select} = require('../estimator');
const Borough = require('../models/borough');
const Phase = require('../models/phase');
const User = require('../models/user');
const Occupation = require('../models/occupation');

const router = new express.Router()

router.post('/createBorough', async (req, res) => { // create borough
    const bor = new Borough(req.body)
    //console.log(req.body)

    try {
        await bor.save();
        //res.status(201).send(user)
        res.status(201).json({
            success: true,
            id: bor._id,
            message: 'Nueva alcaldía registrada exitosamente',
        });
    } catch (error) {
        res.status(400).json({
            success: false,
            error,
            message: 'Error al intentar registrar la nueva alcaldía',
        });
    }
})

// retrieve all users that are registered and do not have an 
// assigned phase, assign them priority and phase or return 0 if no users were found
async function retrieveAndUpdate(idPhase){

    let borUsers = await User.find({assignedPhase:undefined});
    //console.log("Collection of users without an assigned phase", borUsers);

    if(!borUsers){ // no user registries where found
        return 0;
    }

    //console.log("Print Users Profile:\n")
    real_priority(borUsers);
    //console.log("Finished printing!\n")

    for(let b of borUsers){
        const usr = await User.updateOne(
            {_id: b._id}, 
                {
                assignedPhase: idPhase,                    
                priority: b.priority, 
                t:b.t, 
                q:b.q});
    }
    return borUsers.length;
}


// the number of registers is more
// than or equal to the number of vaccines
async function phaseAssignment(data, idPhase, vDate, n_vac){

    let occs = await Occupation.find({});
    let {drop, select} = cut_select(n_vac, data);

    // The selected users for this vaccination phase will have
    // static data from here on out, but must be assigned a phase
    // and a vaccination date

    real_priority(select, occs, 10, 1);
    for(let b of select){
        const usr = await User.updateOne(
            {_id: b._id}, 
                {priority: b.priority,
                schedule:b.schedule,
                assignedPhase: idPhase,
                vaccDate: vDate});
    }

    // The "dropped" registers will be resorted
    real_priority(drop, occs, 10, 0);
    

    for(let b of drop){
        const usr = await User.updateOne(
            {_id: b._id}, 
                {
                schedule:b.schedule,
                t:b.t, 
                q:b.q});
    }
}   

// create a new vaccination wave
// provide a date format like: dd/mm/yyyy (string) 
router.post('/createPhase', async (req, res) => { // create new phase
    // extract elements from request
    const date = req.body.regLimitDate;
    const bName = req.body.boroughName;

    // New vaccinations wave must be added to an existing borough
    const bor = await Borough.findOne({name:bName});
    
    if (!bor) {
        return res
            .status(404)
            .json({ success: false, error: `Se requiere una alcaldía existente para empezar una nueva fase.` })
    } 


    // impossible to create a new phase if borough has an active phase 
    if(bor.id_phase){
        return res
            .status(400)
            .json({success: false, error:"Una fase esta actualmente activa en la alcaldía " + bName});
    }

    // Evaluate if the provided date is possible
    const {limitDate, vaccinationDate} = createDate(date);

    if(!limitDate){
        return res
            .status(400)
            .json({ success: false, error: `La configuración de fechas proporcionada es incorrecta. Imposible crear una nueva fase.` })
    }

    req.body.regLimitDate = limitDate;
    req.body.vaccDate = vaccinationDate;
    req.body.id = bName + "_" + limitDate;

    try {

        let borUsers = await User.find({residenceBorough:bName, assignedPhase:undefined});
        
        if(borUsers.length >= req.body.vaccines){
            console.log("People to be left out ", borUsers.length - req.body.vaccines);
            req.body.pop_vaccinated = req.body.vaccines;
            phaseAssignment(borUsers, req.body.id, req.body.vaccDate, req.body.vaccines);
        } else {
            req.body.pop_vaccinated = borUsers.length;

            // update the borough's wave id
            await Borough.updateOne({_id: bor._id}, {id_phase: req.body.id});
        }
        const ph = new Phase(req.body);
        await ph.save();
        
        res.status(201).json({
            success: true,
            id: ph._id,
            message: 'Nueva fase registrada en la alcaldía ' + bName,
        });
    } catch (error) {
        res.status(400).json({
            success: false,
            error,
            message: 'Error al intentar registrar la nueva fase en la alcaldía',
        });
    }
})


router.get('/getBors', async (req, res) => { // get all boroughs
    await Borough.find({}, (err, bors) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }
        if (!bors.length) { // no borough found
            return res
                .status(404)
                .json({ success: false, error: `No se encontraron alcaldías` })
        }
        return res.status(200).json({ success: true, data: bors })
    }).catch(err => console.log(err)) 
})


// Endpoints to retrieve phases 

router.get('/getPhases', async (req, res) => { // get all phases (historic and current) across all boroughs
    await Phase.find({}, (err, phs) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }
        if (!phs.length) { // no phases found
            return res
                .status(404)
                .json({ success: false, error: `No se encontraron fases` })
        }
        return res.status(200).json({ success: true, data: phs })
    }).catch(err => console.log(err)) 
});

router.get('/getPhases/:borName', async (req, res) => { // get all phases (historic and current) across all boroughs
    await Phase.find({boroughName:req.params.borName}, (err, phs) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }
        if (!phs.length) { // no phases found
            return res
                .status(404)
                .json({ success: false, error: `No se encontraron fases de ` + req.params.borName })
        }
        return res.status(200).json({ success: true, data: phs })
    }).catch(err => console.log(err)) 
});

module.exports = router;