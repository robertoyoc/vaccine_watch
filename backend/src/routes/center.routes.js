const express = require('express');
const Center = require('../models/center');
const Borough = require('../models/borough');
const User = require('../models/user');
const {assignCenter} = require('../estimator');
const router = new express.Router();

async function updateUsersCenters(borUsers, center){
    
    for(let b of borUsers){
        
        if(b.lon && b.lat){ // only users that have a loc can be assigned a center
            const {id_center, address, newDistance} = assignCenter(center,b);

            if(id_center){
                const usr = await User.updateOne(
                    {_id: b._id}, 
                        {assignedCtr: id_center, 
                        ctrAddress: address, 
                        ctrDistance: newDistance
                    }
                );
            }
        }

    }
}

router.post('/createCenter', async (req, res) => { // create vaccination center
    
    let borName = req.body.boroughName;
    const bor = await Borough.findOne({name:borName});
    
    if (!bor) {
        return res
            .status(404)
            .json({ success: false, error: `Esta alcaldía no existe o no fue encontrada. Se requiere una alcaldía existente para registrar un nuevo centro de vacunación.` })
    } 
    
    // create the center id
    req.body.id_center =  borName + '_' + req.body.name;

    try {
        // get all users that haven't accepted their assigned phase
        let borUsers = await User.find({residenceBorough:borName, hasAccepted:0});
        
        console.log("Length of users: ", borUsers.length);

        // update all users in which the new center is closer 
        // to the user in this borough or if the user has no
        // centers assigned
        await updateUsersCenters(borUsers, req.body);

        const center = new Center(req.body);

        await center.save();
        
        return res.status(201).json({
            success: true,
            id: center._id,
            message: 'Nuevo centro de vacunación registrado exitosamente',
        });
    } catch (error) {
        return res.status(500).json({
            success: false,
            error,
            message: 'Error al intentar registrar el centro',
        });
    }
});

router.get('/getCenters/:bName', async (req, res) => { // get all vaccination centers
    await Center.find({boroughName:req.params.bName}, (err, cts) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }
        if (!cts.length) { // no occupations found
            return res
                .status(404)
                .json({ success: false, error: `No se encontraron centros en la alcaldía seleccionada.` })
        }
        return res.status(200).json({ data: cts });
    }).catch(err => console.log(err)) 
});

// endpoint to retrieve a particular vaccination center
router.get('/getCenter/:id_center*', async (req, res) => { 
    await Center.findOne({id_center:req.params.id_center}, (err, ctr) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }
        if (!ctr) { 
            return res
                .status(404)
                .json({ success: false, error: `No se encontro el centro.` })
        }

        return res.status(201).json({ ctr });
    }).catch(err => console.log(err));
});


// update the location of a vaccination center
router.patch('/assignLoc/:id_center*/:lat/:lon', async(req, res)=>{
    
    let center = await Center.findOne({id_center: req.params.id_center});
    
    if(!center){
        return res
            .status(404)
            .json({ success: false, error: `Ningún centro con ese id fue encontrado.` });
    }

    try {

        // check if there are people that have already accepted
        // their phase in this center
        let borUsers = await User.find({hasAccepted:1, hasVaccinated:0, assignedCtr:req.params.id_center});

        if(borUsers.length>0){
            return res
                .status(400)
                .json({ success: false, error: `Imposible actualizar la ubicación de este centro, existen usuarios que ya aceptaron su cita.` });
        }
        
        center.lat = req.params.lat;
        center.lon = req.params.lon;

        await updateUsersCenters(borUsers, center);
        
        await center.save();
        
        return res.status(201).json({
            success: true,
            id: center._id,
            message: 'El centro de vacunación fue actualizado con las nuevas coordenadas exitosamente. ',
        });

    } catch (error) {

        return res.status(500).json({
            success: false,
            error,
            message: 'Error al registrar nuevas coordenadas  al centro con id ' + center.id_center,
        });
    }


});

module.exports = router;