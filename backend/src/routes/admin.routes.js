const express = require('express');
const User = require('../models/user');
const router = new express.Router();

// admin endpoint: check if the given user has
// confirmed its assistance
router.get('/checkUserAttendance/:curp', async(req, res)=>{
    const curp = req.params.curp;

    try {
        const user = await User.findOne({curp:curp});

        if (!user) {
            return res
                .status(404)
                .json({ success: false, error: `Usuario no encontrado` });
        }

        if(!user.hasAccepted){
            return res
                .status(400)
                .json({ success: false, msg: `Usuario no ha confirmado su asistencia.` });
        } 

        // Only returns user object
        return res.status(200).json({success: true, msg: 'Usuario ha confirmado su asistencia.'});

    } catch (e) { // server error
        return res.status(500).send();
    }
});

// admin endpoint: retrieve user by curp 
router.get('/getUserByCurp/:curp', async(req, res)=>{
    const curp = req.params.curp;

    try {
        const user = await User.findOne({curp:curp});

        if (!user) {
            return res
                .status(404)
                .json({ success: false, error: `Usuario no encontrado` })
        }

        // Only returns user object
        return res.status(200).send(user);

    } catch (e) { // server error
        return res.status(500).send();
    }

});

// admin endpoint : change isRequested user tag to true
// pass in the curp of the user and the id of the doctor 
router.patch('/requestUser/:curp/:doctor_id', async(req, res)=>{
    let usr = await User.findOne({curp:req.params.curp});
    let adminUsr = await User.findOne({doctor_id:req.params.doctor_id});

    if(!adminUsr){
        return res
            .status(404)
            .json({ success: false, error: `No se encontró el administrador con este id.` });
    }

    if(!usr){
        return res
            .status(404)
            .json({ success: false, error: `Usuario inexistente o no encontrado.` });
    }

    if(!usr.assignedPhase){
        return res
            .status(400)
            .json({ success: false, error: `Usuario no se le ha asignado una fase de vacunación aún.` });
    }

    // check that the admin is a vaccine administrator and not a supervisor 
    if(adminUsr.position!=='Médico administrador de vacunas'){
        return res
            .status(400)
            .json({ success: false, error: `Este tipo de administrador no puede solicitar certificados.` });
    }

    if(!usr.assignedCtr){
        return res
            .status(400)
            .json({ success: false, error: `Este usuario no tiene un centro de vacunación asignado.` });
    }

    // check that the admin and the user are assigned to the same vacc center
    if(adminUsr.currentCtr!==usr.assignedCtr){
        return res
            .status(400)
            .json({ success: false, error: `Este admin pertenece a otro centro de vacunación.` });
    }

    const dID = req.params.doctor_id;

    try {

        if(usr.isRequested){
            return res
                .status(400)
                .json({success: false, msg: 'El usuario ya ha sido registrado para solicitud de vacunación anteriormente.'});
        }


        // check if the admin is trying to request himself
        if(dID){
            if(dID===usr.doctor_id){
                return res
                .status(400)
                .json({success: false, msg: 'El administrador no puede requerirse a si mismo para certificación.'});
            }
        }

        // perform the updates
        usr.isRequested = 1;

        // change the user field of vaccine administrator to the admin that requested him
        usr.assignedAdminId = dID;

        let sum = adminUsr.approved_counter + 1;
        
        // try to save the user and update the admin counter
        if(await usr.save()){
            await User.findOneAndUpdate({doctor_id:adminUsr.doctor_id}, {approved_counter: sum});
        } else {
            throw 'Error al intentar autorizar al usuario para certificación';
        }

        return res.status(200).json({success:true, msg: 'El usuario ha sido solicitado para certificación satisfactoriamente.'});

    } catch(err){
        return res.status(500).json({success: false, error: err});
    }

});

// admin endpoint: retrieve all users who have both isRequested and userApproved tags
// set to true
router.get('/getRequestingUsers/:ctr*', async (req, res) => { // get all users
    await User.find({assignedCtr: req.params.ctr,
                     isRequested: 1,
                     userApproved:1}, (err, users) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }
        if (!users.length) { // no users found
            return res
                .status(404)
                .json({ success: false, error: `No se encontraron usuarios` })
        } 
        return res.status(200).json({ success: true, data: users })
    }).catch(err => console.log(err)) 
});

// admin endpoint: change a requesting user's isVaccinated tag to true
router.patch('/confirmVaccination/:uuid/:doctor_id', async(req, res)=>{
    try {
        
        let usr = await User.findOne({uuid:req.params.uuid});

        if(!usr)
            return res.status(404).json({success: false, msg: 'Ningun usuario con esta ID fue encontrado.' });


        if(!usr.assignedCtr){
            return res.status(400).json({success: false, msg:"Al usuario no se le ha asignado un centro de vacunación."});
        }

        let adminUsr = await User.findOne({doctor_id:req.params.doctor_id});
        
        if(!adminUsr)
            return res.status(404).json({success: false, msg: 'Ningun usuario administrador con esta ID fue encontrado.' });

        if(adminUsr.position!=='Supervisor de vacunación'){
            return res.status(400).json({success: false, msg: 'Este admin no puede confirmar las vacunaciones.' });
        }

        if(adminUsr.uuid===req.params.uuid){
            return res.status(400).json({success: false, msg: 'El usuario administrador no puede confirmar su propia vacunación.' });
        }

        // check that the admin and the user are assigned to the same vacc center
        if(adminUsr.currentCtr!==usr.assignedCtr){
            return res
                .status(400)
                .json({ success: false, error: `Este admin pertenece a otro centro de vacunación.` });
        }

        if(!usr.userApproved)
            return res.status(400).json({success: false, msg: 'Este usuario aún no confirma su vacunación.' });

        if(!usr.isRequested)
            return res.status(400).json({success: false, msg: 'Este usuario no ha sido solicitado por un admin todavía.'});

        if(usr.isVaccinated)
            return res.status(400).json({success: false, msg: 'Este usuario ya ha sido confirmado anteriomente.'});

        
        // perform the updates to the user and the admin user
        usr.isVaccinated = 1;
        usr.assignedSupervisorId = adminUsr.doctor_id;

        if(await usr.save()){
            let sum = adminUsr.approved_counter + 1;
            await User.findOneAndUpdate({doctor_id:adminUsr.doctor_id}, {approved_counter: sum});
        }

        return res.status(200).json({success: true, msg: 'El usuario fue confirmado satisfactoriamente'});

    } catch(err){
        return res.status(500).json({success: false, error: err});
    }
    
});

// admin endpoint: retrieve all users that were approved by this doctor
router.get('/getUsers/:doctor_id', async (req, res)=>{

    const adminUsr = await User.findOne({doctor_id:req.params.doctor_id});

    let users;
    
    if(adminUsr){
        users = await User.find({
            $or: [
                { assignedAdminId : req.params.doctor_id },
                { assignedSupervisorId : req.params.doctor_id }
              ]
        });

    } else { // if the doctor id was incorrectly inserted, it will return all non-doctor users.
        users = await User.find({doctor_id:undefined});
    }

    if(!users){
        return res.status(404).json({success:false, error:'No se encontraron usuarios'})
    }

    return res.status(200).json({success:true, data: users});

});


router.get('/getUsers', async (req, res)=>{

    try {
        let users = await User.find({doctor_id:undefined});

        if(!users){
            return res.status(404).json({success:false, error:'No se encontraron usuarios'});
        } else {
            return res.status(200).json({success:true, data: users});
        }
    } catch(err){
        return res.status(500).json({success:false, error:"Error del servidor"});
    }

});
    
module.exports = router;