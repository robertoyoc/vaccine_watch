const express = require('express');
const router = new express.Router();
const QRCode = require('qrcode')
const stream = require('stream');
const User = require('../models/user');

const QLDB = require('amazon-qldb-driver-nodejs');
const QldbDriver = QLDB.QldbDriver;
const RetryConfig = QLDB.RetryConfig;

var AWS = require('aws-sdk');
const { json } = require('body-parser');
const { base } = require('../models/user');
AWS.config.update({region:'us-west-2'});
AWS.config.loadFromPath('./config.json');



const LEDGER_NAME = 'mivacuna';
const TABLE_NAME = 'vaccine';

function createQldbDriver(ledgerName = LEDGER_NAME, serviceConfigurationOptions = {}){
    const retryLimit = 4;
    const maxConcurrentTransactions = 10;
    //Use driver's default backoff function (and hence, no second parameter provided to RetryConfig)
    const retryConfig = new RetryConfig(retryLimit);
    const qldbDriver = new QldbDriver(ledgerName, serviceConfigurationOptions, maxConcurrentTransactions, retryConfig);
    return qldbDriver;
}


async function insertDocument(txn, tableName, documents) {
    const statement = `INSERT INTO ${tableName} ?`;
    let result = await txn.execute(statement, documents);
    return result;
}

async function updateAndInsertDocuments(txn, usr) {
    const documentIds = await insertDocument(txn, TABLE_NAME, usr);
    return documentIds;
}



async function sendDatatoQLDB(usr) {
    try {
        const qldbDriver = createQldbDriver();
        await qldbDriver.executeLambda(async (txn) => {
            await updateAndInsertDocuments(txn, usr);
        });
    } catch (e) {
       console.log(`Unable to insert documents: ${e}`);
    }
}


async function validateInQLDB(usrId) {
    try {
        const qldbDriver = createQldbDriver();
        return await qldbDriver.executeLambda(async (txn) => {
            return await findUsers(txn, usrId);
        });
    } catch (e) {
       console.log(`Unable to fetch documents: ${e}`);
    }
}
async function findUsers(txn, usrId) {
    const query = `SELECT * FROM vaccine where id='${usrId}'`

    return await txn.execute(query).then((result) => {
        const resultList = result.getResultList();
        return  (resultList.length)? true: false;
    });
}


// cert generation
// @returns a cert given a user id
router.get('/generate/:uuid', async (req, res)=> {
    try{
        // obtener usuario
        // validar que está vacunado
        // si está hago esto
        // let usr = {
            //     uuid: '16690882-f824-4bca-8e90-8ce1a451fe97',
            //     name: 'roberto',
            //     secondName: 'yoc',
            //     vaccDate: 'today',
            //     vaccType: 'AstraZeneca',
            //     isVaccinated: true
            // }
            
        let usr = await User.findOne({uuid:req.params.uuid});
            
        if (usr.isVaccinated) {
            

            const values = {
                id: usr.uuid,
                fullName: `${usr.name} ${usr.secondName}`
            }
            const cert = JSON.stringify(values)
            let buff = Buffer.from(cert);
            let base64data = buff.toString('base64');
            const isRegistered = await validateInQLDB(usr.uuid);
            if (!isRegistered) {
                console.log('user is not registered, registering')
                const qldbProcess = await sendDatatoQLDB(values);
            }
           
                
            const content = `
            -----BEGIN CERTIFICATE----- 
            ${base64data}
            ----END CERTIFICATE-----
            `;            
            const qrStream = new stream.PassThrough();
            const result = await QRCode.toFileStream(qrStream, content,
                        {
                            type: 'png',
                            width: 200,
                            errorCorrectionLevel: 'H'
                        }
                    );
    
            qrStream.pipe(res);
        } else {
            res.status(400).send('Not vaccinated')
        }
        

        //devuelvo vacío
    } catch(err){
        console.error('Failed to return content', err);
    }
});

router.post('/validate', async (req, res)=> {
    const base64data = req.body.data;
    const buf = Buffer.from(base64data, 'base64').toString('utf-8'); // Ta-da
    const obj = JSON.parse(buf)
    const isReal = await validateInQLDB(obj.id);

    const code = (isReal) ? 200 : 400;
    const message = isReal? 'OK': 'invalid';

    res.status(code).send(message);
    
});


    
module.exports = router;