const express = require('express');
const {real_priority, isDateToday, checkDate, calculateBestCenter, estimated_priority} = require('../estimator');
// const { findById } = require('../models/user');
const User = require('../models/user');
const Borough = require('../models/borough');
const Phase = require('../models/phase');
const Center = require('../models/center');
const Occupation = require('../models/occupation');

const router = new express.Router()

// endpoint to create user and update all users
router.post('/createUser', async (req, res) => { // create user 

    // first step : retrieve the borough name, check if exists
    let borName = req.body.residenceBorough;
    const bor = await Borough.findOne({name:borName});
    
    if (!bor) {
        return res
            .status(404)
            .json({ success: false, error: `Esta alcaldía no existe o no fue encontrada. Se requiere una alcaldía existente para registrar al usuario.` })
    } 


    // second step: Check if the user is an admin user
    if(req.body.position === 'Médico administrador de vacunas' || req.body.position === 'Supervisor de vacunación'){
        
        if(!req.body.doctor_id){
            return res
                .status(400)
                .json({ success: false, error: `Se necesita un id de médico administrador para crear un usuario administrador.`});
        }

        // enforce uniqueness
        const queryAdmin = await User.findOne({doctor_id:req.body.doctor_id});

        if(queryAdmin){
            return res
                .status(400)
                .json({ success: false, error: `Ya existe un administrador con ese id`});
        }

        if(!req.body.currentCtr){
            return res
                .status(400)
                .json({ success: false, error: `Se necesita asignar a este administrador a un centro específico.`});
        }

        const adminCenter = await Center.findOne({id_center:req.body.currentCtr});
        
        if(!adminCenter){
            return res
                .status(400)
                .json({ success: false, error: `No se encontró el centro de vacunación especificado.`});
        }
    } 

    // third step: Assign a vaccination center if there is one
    let ctrs = await Center.find({boroughName:borName});

    if(req.body.lat && req.body.lon && ctrs !== undefined && ctrs.length !== 0){
        
        const {bestCtr, bestDist} = calculateBestCenter(ctrs, req.body);

        req.body.assignedCtr = bestCtr.id_center;
        req.body.ctrAddress = bestCtr.address;
        req.body.ctrDistance = bestDist;
    }

    console.log(req.body);

    // fourth step: register user and call sorting algorithm 
    if(!req.body.age || !req.body.position){
        return res
                .status(400)
                .json({ success: false, error: `Campos como edad u ocupación deben llenarse.`});
    }

    req.body.priority = estimated_priority(req.body.age, req.body.position);
    const user = new User(req.body);    

    try {
        if(await user.save()){
            await Borough.updateOne({_id:bor._id}, {pop:bor.pop+1});
            
        } else {
            throw "Imposible guardar al usuario.";
        }
        
        // Check if there is an active phase in the borough
        if(bor.id_phase){
            console.log("Found active phase");
            const ph = await Phase.findOne({id:bor.id_phase});
            if((ph.vaccines === ph.pop_vaccinated + 1) || isDateToday(ph.regLimitDate)){ 

                // the limit or date has been met, update all registers to the active phase
                await update_AssignAll(borName, ph.id, ph.vaccDate);
                
                // deactivate the phase in the borough
                await Borough.updateOne({_id:bor._id}, {id_phase:undefined});

            } else {
                await updateAll(borName);
            }
            // Add 1 to the counter of the active phase 
            await Phase.updateOne({_id:ph._id}, {pop_vaccinated: ph.pop_vaccinated + 1});
        } else {
            console.log("Active phase not found");
            await updateAll(borName);
        }

        res.status(201).json({
            success: true,
            id: user._id,
            message: 'Usuario registrado exitosamente',
        });
    } catch (error) {
        res.status(400).json({
            success: false,
            error,
            message: 'Error al intentar registrar el nuevo usuario',
        });
    }
});

router.post('/login', async(req, res)=>{ // login user
    try {
        const user = await User.findByCredentials(req.body.email, req.body.password)
        res.status(200).json({
            success:true,
            data: user
        })
    } catch (e){
        res.status(400).json({
            success: false,
            //error,
            message: 'Error en usuario/contraseña',
        });
    }
});

router.get('/getUsers', async (req, res) => { // get all users
    await User.find({}, (err, users) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }
        if (!users.length) { // no users found
            return res
                .status(404)
                .json({ success: false, error: `No se encontraron usuarios` })
        } 
        return res.status(200).json({ success: true, data: users })
    }).catch(err => console.log(err)) 
})

// Endpoint to retrieve all users by borough
router.get('/getUsers/:bor', async (req, res) => { // get all users
    await User.find({residenceBorough: req.params.bor}, (err, users) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }
        if (!users.length) { // no users found
            return res
                .status(404)
                .json({ success: false, error: `No se encontraron usuarios` })
        } 
        return res.status(200).json({ success: true, data: users })
    }).catch(err => console.log(err)) 
})

async function update_AssignAll(borName, idPhase, vaccDate){
    console.log("Updating all users");
    let borUsers = await User.find({residenceBorough:borName, assignedPhase:undefined});
    let occs = await Occupation.find({});

    /* console.log("Collection of users in ", borName, " that don't have an assigned phase :");
    console.log(borUsers); */
    if(!borUsers){ // no user registries where found
        return 0;
    }

    // There was a last user registered, update all user priorities
    // this last resorting can be averted
    //console.log("Print Users Profile:\n")
    real_priority(borUsers, occs,10, 1);
    //console.log("Finished printing!\n")

    // Update all users and assign the completed phase
    for(let b of borUsers){
        const usr = await User.updateOne(
            {_id: b._id}, 
                {priority: b.priority, 
                schedule:b.schedule,
                t:b.t, 
                q:b.q,
                assignedPhase: idPhase, 
                vaccDate: vaccDate
            }
        );
    }
    return borUsers;
}
// Returns 0 if no users were found
// Only updates users that have no 
// assigned phase
async function updateAll(borName){

    
    let borUsers = await User.find({residenceBorough:borName, assignedPhase:undefined});
    
    let occs = await Occupation.find({});

    // console.log("Collection of users in ", borName, " that don't have an assigned phase :");
    // console.log(borUsers);
    if(!borUsers){ // no user registries where found
        return 0;
    }
    
    // call sorting algorithm, but tell it to don't replace priorities
    real_priority(borUsers, occs, 10, 0);
    
    // no need to update the priority now,
    // the NN will handle that
    for(let b of borUsers){
        const usr = await User.updateOne(
            {_id: b._id}, 
                {
                schedule:b.schedule,
                t:b.t, 
                q:b.q
                });
    }
    
    return borUsers;
}

// endpoint to allow users to confirm their attendance
router.patch('/confirmAttendance/:uuid/:response', async(req, res)=>{

    const uuid = req.params.uuid;
    const response = req.params.response;

    console.log(response);

    // try update 
    try {

        const user = await User.findOne({uuid:uuid});

        if(!user){
            return res
                .status(404)
                .json({ success: false, error: `Usuario no encontrado` });
        }


        if(user.hasAccepted){
            return res
                .status(400)
                .json({ success: false, error: `Usuario ya ha confirmado su asistencia anteriormente.` });
        }

        // confirm if the user has a vaccination phase assigned 
        if(!user.vaccDate){
            return res
                .status(404)
                .json({ success: false, error: `Usuario aun no tiene una fecha de vacunación asignada.` });
        }

        // check if the vaccination date isn't today or hasn't already passed
        if(!checkDate(user.vaccDate)){

            // remove all of the vaccination info
            user.vaccDate = undefined;
            user.assignedPhase = undefined;
            user.hasAccepted = 0;

            await user.save();

            return res
                .status(400)
                .json({ success: false, error: `Usuario no puede confirmar su asistencia en el mismo día de vacunación.` });
        }


        // do the appropriate updates to the
        // user object 
        if(response===undefined||response==='0'){
            console.log("User refused");
            // update the closed phase to reflect the
            // updated number of vaccinated people
            let phase = await Phase.findOne({id: user.assignedPhase});

            phase.pop_vaccinated = phase.pop_vaccinated - 1;

            await phase.save();

            // all of the vaccination info will be removed,
            // hence the user will be able to be elected 
            // for another vaccination phase later
            user.vaccDate = undefined;
            user.assignedPhase = undefined;
            user.hasAccepted = 0;

        } else {
            console.log("User accepted");
            user.hasAccepted = 1;
        }

        await user.save();

        return res.status(200).json({ success: true, data: user })

    } catch(e){
        // server related issue
        return res.status(500).send(e);
    }
});

// endpoint to retrieve all users of a given vaccination phase that have confirmed their assistance
router.get('/getConfirmedUsers/:phaseId*/:m/:y', async(req, res)=>{
    
    const phaseID = req.params.phaseId + '/' + req.params.m + '/' + req.params.y;
    
    await User.find({assignedPhase: phaseID, hasAccepted:1}, (err, users) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }
        if (!users.length) { // no users found
            return res
                .status(404)
                .json({ success: false, error: `No se encontraron usuarios` })
        } 
        return res.status(200).json({ success: true, data: users })
    }).catch(err => console.log(err)) 
});


// Endpoint to perform priority assignation manually, without inserting new user
router.patch('/usersAssignation/:borName', async (req, res) => { // find user by id
    
    try { // find borough
        
        let resp = await updateAll(req.params.borName);

        if(resp!=0){
            //res.status(200).send(resp);
            res.status(200).json({ success: true, msg: "Usuarios actualizados" });
        } else {
            return res
                .status(404)
                .json({ success: false, error: `No hay usuarios registrados para esta fase` })
        }   
    } catch (e) { // server error
        console.log(e)
        res.status(500).send()
    }
})

// endpoint to assign the user's current coordinates
// NOTE: this must be the coordinates for the user's permanent residence
router.patch('/assignLocation', async(req, res)=>{
    const usr_lat = req.body.lat;
    const usr_lon = req.body.lon;
    const uuid = req.body.uuid;

    if(!usr_lat || !usr_lon){
        return res
            .status(400)
            .json({ success: false, error: `No se proporcionaron las coordenadas` });
    }

    if(!uuid){
        return res
            .status(400)
            .json({ success: false, error: `No se proporció el cognito id del usuario` });
    }

    // TODO: Check if user has no lat and lon already assigned

    // Update the user based on its cognito uuid
    const usr = await User.updateOne(
        {uuid: uuid}, 
            {lat: usr_lat, 
            lon: usr_lon,});

    return res.status(200).json({success:true, data:usr});
});


// endpoint to allow a user to confirm the vaccination has taken place
router.patch('/userConfirmation/:uuid', async(req, res)=>{
    let usr = await User.findOne({uuid:req.params.uuid});

    if(!usr){
        return res
            .status(400)
            .json({ success: false, error: `Usuario inexistente o no encontrado.` });
    }

    try {

        if(usr.userApproved){
            return res
                .status(400)
                .json({success: false, msg: 'El usuario ya ha confirmado su vacunación anteriormente.'});
        }

        usr.userApproved = 1; 
        await usr.save();

        return res.status(200).json({success:true, msg: 'Has confirmado tu vacunación exitosamente.'});

    } catch(err){
        return res.status(500).json({success: false, error: err});
    }

});

// Retrieve a user with its cognito id
router.get('/getUser/:uuid', async (req, res) => { // find user by id
    const uuid = req.params.uuid

    try {
        const user = await User.findOne({uuid:uuid});

        if (!user) {
            return res
                .status(404)
                .json({ success: false, error: `Usuario no encontrado` })
        }

        // Only returns user object
        res.status(200).send(user);

    } catch (e) { // server error
        res.status(500).send()
    }
})


// update user from uuid from cognito
router.patch('/updateUser/:uuid', async (req, res)=>{ // update user by id

    // check if body fields provided can be updated
    const updates = Object.keys(req.body);
    const allowedUpdates = ['name', 'email', 'password', 'age', 'phone', 'position'];
    const isValidOperation = updates.every((update)=>allowedUpdates.includes(update));

    if(!isValidOperation){
        return res
            .status(400)
            .json({success: false, error : 'Nuevos campos no pueden crearse y ciertos otros no pueden alterarse.'});
    }

    // try update 
    try {
        // runvalidators shouldn't be necessary since validation happens in frontend.
        //const user = await User.findByIdAndUpdate(req.params.id, req.body, {new:true, runValidators: true})
        const user = await User.findOne({uuid:req.params.uuid});

        updates.forEach((update)=>user[update] = req.body[update]);
        await user.save();


        if(!user){
            return res
                .status(404)
                .json({ success: false, error: `Usuario no encontrado` });
        }

        res.status(200).json({ success: true, data: user })
    } catch(e){
        // server related issue
        // validator related issue
        res.status(400).send(e);
    }
})


// delete user with uuid from cognito
router.delete('/deleteUser/:uuid', async(req, res)=>{

    const user = await User.findOne({uuid: req.params.uuid});

    if (!user) {
        return res
            .status(404)
            .json({ success: false, error: `Usuario no encontrado` })
    }

    // update the population of the borough
    const bor = await Borough.findOne({name:user.residenceBorough});
    bor.pop = bor.pop - 1;
    await bor.save();

    await User.deleteOne({uuid:user.uuid});
    res.status(200).json({ success: true, data: user })
});
    


module.exports = router;