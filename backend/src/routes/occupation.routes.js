const express = require('express');
const Occupation = require('../models/occupation');
//const User = require('../models/user');

const {occNames} = require('../estimator');
const router = new express.Router()

router.post('/createOccupation', async (req, res) => { // create occupation
    const occ = new Occupation(req.body)
    
    try {
        await occ.save();
        //res.status(201).send(user)
        res.status(201).json({
            success: true,
            id: occ._id,
            message: 'Nuevo trabajo registrado exitosamente',
        });
    } catch (error) {
        res.status(400).json({
            success: false,
            error,
            message: 'Error al intentar registrar el nuevo trabajo',
        });
    }
})


router.get('/getOccupations', async (req, res) => { // get all occupations
    await Occupation.find({}, (err, occs) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }
        if (!occs.length) { // no occupations found
            return res
                .status(200)
                .json({ data: occNames });
        }
        let occsNames = occs.map(({name})=>name);
        return res.status(200).json({ data: occsNames })
    }).catch(err => console.log(err)) 
})

module.exports = router;