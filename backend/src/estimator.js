const brain = require('brain.js');
const {train_data, t_data} = require('./trainingData');

// instantiate neural network
const net = new brain.NeuralNetwork({
    hiddenLayers: [3, 4],
    activation: 'sigmoid'
});

// train the network, print the MSE
console.log("Training NN...");
const stats = net.train(t_data);
console.log(".........Done!");
console.log(stats);

// utilites to display the network architecture
/* console.log(brain.utilities.toSVG(net)); */
/* console.log(net.run([ 1,0,0,0,0, 21.0])); */

// try network
//let r = net.run({ position: 0.5, age:0.8387096774193549});
//let r = net.run({ position: 1, age:0.3064516129032258});
//{ position: 6, age:0.14516129032258066} //8
//console.log(r);
//console.log(Object.keys(r).reduce((a, b) => r[a] > r[b] ? a : b));

function sortNumber(a,b) {
    return a - b;
}

function quantile(array, percentile) {
    array.sort(sortNumber);
    index = percentile/100. * (array.length-1);
    if (Math.floor(index) == index) {
    	result = array[index];
    } else {
        i = Math.floor(index)
        fraction = index - i;
        result = array[i] + (array[i+1] - array[i]) * fraction;
    }
    return result;
}

function quantile_on_obj(array, percentile) {
    index = percentile/100. * (array.length-1);
    if (Math.floor(index) == index) {
    	result = array[index];
    } else {
        i = Math.floor(index)
        fraction = index - i;
        result = array[i] + (array[i+1] - array[i]) * fraction;
    }
    return result;
}

const percentile = (arr, val) =>
  (100 *
    arr.reduce(
      (acc, v) => acc + (v < val ? 1 : 0) + (v === val ? 0.5 : 0),
      0
    )) / arr.length;

/*
qArray.forEach(element => {
    console.log("start:", element.start);
    console.log("end:", element.end);
});
*/
// simulate ages of population 
        /*
        let distribution = []
        for(let i=0; i<pop; i++){
            let newElem = Math.floor(randn_bm(10, 100, 1))
            distribution.push(newElem)
            console.log(newElem)
        }
        */

// this function queries the trained NN
// to produce a label that represents the
// priority level
exports.estimated_priority = function(age, occupation){

    // certain occupations should be treated as doctors
    if (occupation==='Médico administrador de vacunas' || occupation==='Supervisor de vacunación') {
        occupation = 'Médico';
    } 

    // constants derived from training data
    const min = 18;
    const max = 80;

    // normalize features
    if(age>max)
        age = max;
    
    if(age<min)
        age = min;
    
    age = (age - min)/(max-min);

    let encode;

    switch(occupation){
        case 'Médico':
            encode = 10;
            break;
        case 'Docente':
            encode = 7;
            break;
        case 'Público':
            encode = 6;
            break;
        case 'Comida/Construcción':
            encode = 5;
            break;
        case 'Estudiante':
            encode = 3;
            break;
        case 'otros':
            encode = 2;
            break;  
        default:                
            encode = 2;
    }
    let normalized_weight = (encode - 2)/(10-2);
    
    // query the NN
    console.log('Querying the network with data:');
    console.log('position: ', normalized_weight, ' age:', age);
    const res = net.run({ position: normalized_weight, age:age});

    // only return the label that got the highest score
    const ans = Object.keys(res).reduce((a, b) => res[a] > res[b] ? a : b);

    console.log("Predicted priority: ", ans);

    return parseInt(ans);
}

/* console.log(estimated_priority(46, 'Público')) */

// Transform ages with occupation data to produce a new factor for each user
// These are the default weights if no other are given.
let occs = new Object();
// Weights
occs['Médico'] = 1.5;
occs['Docente'] = 1.25;
occs['Público'] = 1.2;
occs['Comida/Construcción'] = 1.15;
occs['otros'] = 1.00;
occs['Estudiante'] = 1.1;
// Admin Weights
occs['Médico administrador de vacunas'] = occs['Médico'];
occs['Supervisor de vacunación'] = occs['Médico'];


// Schedules
let scheds = new Object();

scheds[1] = '8:00 am - 10:59 am';
scheds[2] = scheds[1];
scheds[3] = '11:00 am - 13:59 pm';
scheds[4] = scheds[3];
scheds[5] = '14:00 pm - 16:59 pm';
scheds[6] = scheds[5];
scheds[7] = '17:00 pm - 19:59 pm';
scheds[8] = scheds[7];
scheds[9] = '20:00 pm - 23:59 pm';
scheds[10] = scheds[9];

const occNames = Object.keys(occs);
console.log("Loaded default occupation weigths.");
console.log(occNames);
exports.occNames = [...occNames];

// Given an array of objects, return priority and "t" factor for all elements
// Returns modified array of objects with these new fields: priority, t, schedule
exports.real_priority = function(data, occupations, nPriorityLvls = 10, replacePriority=1){

    // Get data length
    const n = data.length;
    
    console.log("Number of registries found: ", n, "\n");
    
    if(n<10){ // Deal with cases with few registries
        console.log("Current population is small, changing priority levels to compensate...\n");
        nPriorityLvls = n;
    }

    // verify if there are occupations 
    let occObj = new Object();
    if(occupations.length>0){
        console.log("Using found weights");
        occupations.forEach(x=>{
            occObj[x.name] = x.priority;
        });
    } else {
        console.log("Using default weights");
        // if none are found, use the default weights
        occObj = occs;
    }

    // Execute the transformations
    data.forEach(element => {
        // Calculate transformation TODO: port function
        
        if(occObj[element.position]!==undefined){
            element.t = element.age * occObj[element.position];
        } else {
            element.t = element.age;
        }            
        
        // Print
        //console.log("Name: ", element.name," Age: ", element.age," Occupation: ", element.position," factor: ", element.t);
    });

    // sort on t
    data.sort((a, b) => (a.t > b.t) ? 1 : (a.t === b.t) ? ((a.t > b.t) ? 1 : -1) : -1 )
    
    // Print the sorted array on t
    //console.log("\nSorted array based on t:\n");

    // Store each t factor element into a separate array to perform the percentile calculation
    // we don't have to perform sorting each time for the data array of objects is sorted already
    let arrT = [];
    for(let i=0; i<n; i++){
        arrT.push(data[i].t); 
        data[i].q = ((i + 1) / n).toFixed(2);
        //console.log("Name: ", data[i].name," Age: ", data[i].age," Occupation: ", data[i].position," factor: ", data[i].t, " above q:", data[i].q);
    }
       
    // get the percentiles of the data according to the number of priority levels desired
    let factor = 100;
    deciles = []
    let a = factor/(nPriorityLvls)
    for(let i=a; i<factor+1; i=i+a){
        //console.log(i);
        deciles.push(i);
    }

    // Get the boundaries for each decile or percentile
    let qArray = [];
    qArray.push(new Object());
    qArray[0].start = quantile_on_obj(arrT, 100-deciles[0]);
    qArray[0].end = 200;
    
    for(let i=1; i<deciles.length; i++){
        qArray[i] = new Object();
        qArray[i].end = qArray[i-1].start;
        qArray[i].start = Math.floor(deciles[i])!=100? quantile_on_obj(arrT, 100-deciles[i]):quantile_on_obj(arrT, 0)
       }
    
    // Print the boundaries for each decile
    /* qArray.forEach(e=>{
        console.log("end", e.end);
        console.log("start", e.start);
    }) */
    
    // Iterate through each decile boundary to determine where datapoint 'p' belongs to
    let prior = (p) => {
        let prLevel = 1;
        for(let i=0; i<qArray.length; i++){
            if(p>=qArray[i].start && p<qArray[i].end){
                break;
            }
            prLevel++;
        }
        return prLevel;
    }
    
    // Iterate through all elements to assign to each a priority with t factors
    data.forEach(e=>{
        if(replacePriority)
            e.priority = prior(e.t);
        
        e.schedule = scheds[e.priority];
    });

    //data.forEach(e=>console.log("Name: ", e.name, " Age: ", e.age, " Factor: ", e.t, " Priority: ", e.priority))

    return data;
}

/* let objs_dummy = [];
    
objs_dummy[0] = {name:"Ed", position: "médico", age: 39}
objs_dummy[1] = {name:"Edu", position: "público", age: 47}
objs_dummy[2] = {name:"Jimena", position: "docente", age: 27}
objs_dummy[3] = {name:"Arturo", position: "otros", age: 34}
objs_dummy[4] = {name:"Goro", position: "comida/construcción", age: 78}
objs_dummy[5] = {name:"Anna", position: "docente", age: 45}
objs_dummy[6] = {name:"Karen", position: "otros", age: 50}
objs_dummy[7] = {name:"Richard", position: "docente", age: 57}
objs_dummy[8] = {name:"Jan", position: "docente", age: 38}
objs_dummy[9] = {name:"Juez", position: "otros", age: 68}
objs_dummy[10] = {name:"Judy", position: "docente", age: 43}
    
objs_dummy = real_priority(objs_dummy);
let {drop ,select} = cut_select(11, objs_dummy);

console.log("Drop these columns:");
drop.forEach(e=>console.log("Name: ", e.name, " Age: ", e.age, " Factor: ", e.t, " Priority: ", e.priority))

console.log("Select these columns:");
select.forEach(e=>console.log("Name: ", e.name, " Age: ", e.age, " Factor: ", e.t, " Priority: ", e.priority))

console.log("Rearrange dropeed columns.")
drop = real_priority(drop); */

exports.cut_select = function(vaccines, data){
    data.sort((a, b) => (a.t > b.t) ? 1 : (a.t === b.t) ? ((a.t > b.t) ? 1 : -1) : -1 );
    let cutoff = data.length - vaccines;
    let drop = data.slice(0, cutoff);
    let select = data.slice(-vaccines);
    return {drop, select}
}

/* let numArray = [0 , 1, 2, 3, 5, 6, 7, 8, 9, 10];
let {drop ,select} = cut_select(3, numArray)
console.log( "Drop array: ", drop);
console.log( "Select array: ", select); */

/// DATE UTILS ///////////////////////////////////////////////////////

Date.prototype.addDays = function(days) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
}

// Returns true if the provided date is 
// still ahead of the current date, format : dd/mm/yyyy (str)
exports.checkDate = function(strDate){
    strDate = strDate.split("/").map(x=>parseInt(x));
    // Subtract 1 from month to account for the index 0
    strDate[1] = strDate[1] - 1; 
    strDate = new Date(strDate[2], strDate[1], strDate[0],0,0,0,0);
    currDate = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
    return strDate>currDate;
}

// Returns true if the provided string date is today
exports.isDateToday = function(strDate){
    strDate = strDate.split("/").map(x=>parseInt(x));
    strDate[1] = strDate[1] - 1; 
    strDate = new Date(strDate[2], strDate[1], strDate[0],0,0,0,0);
    let currDate = new Date();
    return strDate.getFullYear()===currDate.getFullYear() && 
    strDate.getMonth() === currDate.getMonth() && strDate.getDate()=== currDate.getDate();
}

//console.log(isDateToday("30/04/2021"));


// format for dates: dd/mm/yyyy (string)
// return values names:  limitDate, vaccinationDate
exports.createDate = function(regLimitDate, vaccDate){
    // If no limit date is provided for the registration
    // phase, one will be created in a week time. 

    let currentDate = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());

    if(!regLimitDate && !vaccDate){
        console.log("None were provided");
        regLimitDate = new Date().addDays(7)
        console.log(regLimitDate);
        vaccDate = new Date().addDays(10);
    } else if (!vaccDate){
        regLimitDate = regLimitDate.split("/").map(x=>parseInt(x));
        // Subtract 1 from month to account for the index 0
        regLimitDate[1] = regLimitDate[1] - 1; 
        regLimitDate = new Date(regLimitDate[2], regLimitDate[1], regLimitDate[0],0,0,0,0);
        vaccDate = new Date(regLimitDate.getFullYear(), regLimitDate.getMonth(), regLimitDate.getDate(),0,0,0,0).addDays(3);
        //console.log(regLimitDate, vaccDate);
    } else if (!regLimitDate) {
        vaccDate = vaccDate.split("/").map(x=>parseInt(x));
        vaccDate[1] = vaccDate[1] - 1; 
        vaccDate = new Date(vaccDate[2], vaccDate[1], vaccDate[0], 0, 0, 0, 0);

        // impossible to create a registration limit date
        // if the vaccination date provided is today, before today or tomorrow
        let tomorrow = currentDate.addDays(1);
        if(currentDate >= vaccDate || (tomorrow.getDate()===vaccDate.getDate() && tomorrow.getMonth()===vaccDate.getMonth()
        && tomorrow.getFullYear()===vaccDate.getFullYear())){
            return {undefined, undefined};
        }
        
        regLimitDate = new Date(vaccDate.getFullYear(), vaccDate.getMonth(), vaccDate.getDate(),0,0,0,0);
        regLimitDate.setDate(vaccDate.getDate() - 3);

    } else {
        // TODO: no date verification is currently in place
        regLimitDate = regLimitDate.split("/").map(x=>parseInt(x));
        // Subtract 1 from month to account for the index 0
        regLimitDate[1] = regLimitDate[1] - 1; 
        regLimitDate = new Date(regLimitDate[2], regLimitDate[1], regLimitDate[0],0,0,0,0);

        vaccDate = vaccDate.split("/").map(x=>parseInt(x));
        // Subtract 1 from month to account for the index 0
        vaccDate[1] = vaccDate[1] - 1; 
        vaccDate = new Date(vaccDate[2], vaccDate[1], vaccDate[0],0,0,0,0);        
    }

    // verify if date provided is possible
    // registration limit cannot be today
    if(currentDate.getDate()===regLimitDate.getDate() && currentDate.getMonth()===regLimitDate.getMonth() // limit date must be at least 1 day from now
     && currentDate.getFullYear() === regLimitDate.getFullYear() || currentDate > regLimitDate || regLimitDate >= vaccDate){ 
        console.log("Dates provided are not correct");
        return {undefined, undefined};
    }

    let dd = String(regLimitDate.getDate()).padStart(2, '0');
    let mm = String(regLimitDate.getMonth() + 1).padStart(2, '0');
    let yyyy =  regLimitDate.getFullYear();
    const limitDate = dd + '/' + mm + '/' + yyyy;
    console.log("Registration date", regLimitDate);
    dd = String(vaccDate.getDate()).padStart(2, '0');
    mm = String(vaccDate.getMonth() + 1).padStart(2, '0');
    yyyy =  vaccDate.getFullYear();
    const vaccinationDate = dd + '/' + mm + '/' + yyyy;
    console.log("Vaccination date", vaccDate);
    return {limitDate, vaccinationDate};
}
/* 
const {limitDate, vaccinationDate} = createDate('02/05/2021', '01/05/2021');
console.log("Limit date", limitDate);
console.log("Vaccination date", vaccinationDate); */

//createPhase("29/04/2021");

////////////////////////////// GEOLOCATION UTILS

function toRad(Value){return Value * Math.PI / 180}

function getDistance(latCenter, lonCenter, latUser, lonUser){
    /* console.log('latcenter: ', latCenter);
    console.log('loncenter: ', lonCenter);
    console.log('latUser: ', latUser);
    console.log('lonUser: ', lonUser); */
    var R = 6371;
    var dLat = toRad(latUser-latCenter);
    var dLon = toRad(lonUser-lonCenter);
    var lat1 = toRad(latCenter);
    var lat2 = toRad(latUser);

    var a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 

    return  R * c;
}

// centers : array of center objects
// user : user object 
// returns the best center object and the best distance
exports.calculateBestCenter = function(centers, user){

    if(!centers || !user){
        return {undefined, undefined};
    }

    let distances = [];
    let ctrs = [];
    for(let i=0; i<centers.length; i++){
        distances[i] = getDistance(centers[i].lat, centers[i].lon, user.lat, user.lon);
        console.log(distances[i]);
        ctrs[i] = centers[i];
        if(i!=0 && distances[i]>=distances[i-1]){
            let temp = distances[i];
            distances[i] = distances[i-1];
            distances[i-1] = temp; 

            let temp2 = ctrs[i];
            ctrs[i] = ctrs[i-1];
            ctrs[i-1] = temp2;
        }
    }
    let bestDist = distances[distances.length-1];
    let bestCtr = ctrs[ctrs.length-1];
    /* console.log("The closest center for this user is " + bestCtr.name); */
    return {bestCtr, bestDist};
}

/* req.body.assignedCtr = bestCtr.id_center;
        req.body.ctrAddress = bestCtr.address;
        req.body.ctrDistance = bestDist; */

// update user's vaccination center
exports.assignCenter = function(newCenter, user){

    /* console.log("user lat: ", user.lat)
    console.log("user lon: ", user.lat)
    console.log("center lat: ", newCenter.lat)
    console.log("center lon: ", newCenter.lon) */
    const newDistance = getDistance(newCenter.lat, newCenter.lon, user.lat, user.lon);
    const id_center = newCenter.id_center;
    const address = newCenter.address;

    /* console.log("Distance to new center: ", newDistance);
    console.log("Id of new center: ", id_center);
    console.log("Adress of the new center: ", address); */
    
    if(user.CtrDistance===undefined || newDistance<user.CtrDistance){
        return {id_center, address, newDistance};
    }

    return {undefined, undefined, undefined};
}

/* let macor = {name:"macor", lat: 19.39050488919061, lon:-99.29139256188101};
let fresko = {name:"fresko", lat: 19.389120846935363, lon:-99.2927705131388};
let shuky = {name:"shuky", lat:19.382537458669596, lon:-99.29450571102954};
let panorama ={name:"panorama",lat:19.39469292326946, lon:-99.2901384074004};

let centers  = [fresko, shuky, panorama];

const {bestCtr, bestDist} = calculateBestCenter(centers, macor);
console.log(bestCtr);
console.log(bestDist); */


