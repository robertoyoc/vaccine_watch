const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const userSchema = new mongoose.Schema({
    uuid:{  // field for the cognito id
        type: String, 
        trim: true,
        unique: true, 
        required: true
    },
    name: {
        type: String,
        required: true,
        trim: true
    },
    secondName: {
        type: String,
        required: true,
        trim: true
    },
    residenceBorough:{
        type: String,
        required: true        
    },
    // Properties to be updated when priorizing users
    t:{ // property to store the occupation transformation
        type: Number
    },
    priority:{
        type: Number
    },
    q:{
        type: Number
    },
    ////// Properties to tell if user was elected for vaccination
    ////// and if they are vaccinated (this last one has to be manually changed by an admin)
    assignedPhase:{
        type: String
    },
    vaccDate:{
        type: String
    },
    hasAccepted:{ // flag to tell if the user has accepted the assigned vaccination phase
        type: Number, 
        default: 0
    },
    isRequested:{
        type: Number,
        default: 0
    },
    userApproved:{
        type:Number,
        default: 0
    },
    isVaccinated:{
        type: Number,
        default: 0
    },
    /////////////////////
    age: {
        type: Number
    },
    sex:{
        type: String
    },
    curp:{
        type:String,
        unique: true, 
        required: true
    },
    email: {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    phone:{
        type: String, 
        required: true
    },
    position:{ // in case of admin, this will determine which kind of admin
        type: String, 
        required: true
    },
    password: {
        type: String,
        required: true,
        trim: true
    },
    lat:{ // coordinates
        type: Number
    }, 
    lon:{
        type:Number
    },
    // information about the assigned vacc center
    assignedCtr:{
        type: String
    },
    ctrAddress : {
        type: String,
        default: "Centro de vacunación no definido."
    },
    ctrDistance:{
        type: Number
    },
    schedule:{ // field for schedule of user's vaccination 
        type: String,
        default: 'Horario no definido.'
    },
    // id string for its assgined vaccination center
    id_center:{
        type: String
    },
    // fields for admin user
    currentCtr:{
        type: String
    },
    approved_counter:{ // in case of an admin doctor, it is the counter of people requested for certifications.
        type: Number,    // in case of an supervisor doctor, it is the counter of people certified for vaccination.
        default:0
    },
    // field for doctor id (null in case of user) and 
    // field for assigned admin doctor id (null in case of doctor supervisor)
    // and field for assigned supervisor doctor id (null in case of doctor admin)
    doctor_id:{
        type: String
    },
    assignedAdminId:{
        type: String
    },
    assignedSupervisorId:{
        type: String
    },
    vaccType:{ // field to store the type of vaccine administered
        type: String
    }    
});


userSchema.statics.findByCredentials = async(email, password)=>{
    const user = await User.findOne({email});

    if(!user){
        throw new Error('Unable to login');
    }

    const isMatch = await bcrypt.compare(password, user.password)

    if(!isMatch){
        throw new Error("Unable to login");
    }

    return user;
}

// Before saving, hash user password
userSchema.pre('save', async function(next){ // arrow functions do not work 
    const user = this;

    if(user.isModified('password')){
        user.password = await bcrypt.hash(user.password, 8);
    }

    next()
});

// Data verification occurs in frontend
const User = mongoose.model('User', userSchema);

module.exports = User;