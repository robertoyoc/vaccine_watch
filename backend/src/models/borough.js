const mongoose = require('mongoose');

const boroughSchema = new mongoose.Schema({
    name: {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    id_phase: { // this string represents the current vaccination wave 
                // allocated to the borough
        type: String
    },
    pop:{
        type: Number, 
        default: 0
    }
})

const Borough = mongoose.model('Borough', boroughSchema);

module.exports = Borough;