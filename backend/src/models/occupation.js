const mongoose = require('mongoose');

const occupationSchema = new mongoose.Schema({
    name: {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    priority: {
        type: Number
    }
})

const Occupation = mongoose.model('Occupation', occupationSchema);

module.exports = Occupation;