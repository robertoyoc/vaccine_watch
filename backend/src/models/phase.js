const mongoose = require('mongoose');

const phaseSchema = new mongoose.Schema({
    id:{ // boroughName_regLimitDate
        type:String, 
        unique: true
    },
    boroughName:{
        type:String,
        required: true
    },
    regLimitDate: {
        type: String      
    },
    vaccDate:{
        type: String
    },
    vaccines: {
        type: Number,
        required: true
    },
    pop_vaccinated:{
        type: Number,
        default: 0
    }
})

const Phase = mongoose.model('Phase', phaseSchema);

module.exports = Phase;