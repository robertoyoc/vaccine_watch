const mongoose = require('mongoose');

const centerSchema = new mongoose.Schema({
    name: { // name of the vaccination center
        type: String,
        required: true,
        trim: true
    },
    boroughName:{ // borough in which center is located
        type:String,
        required: true
    },
    id_center: { // Combination of name of center
                 // with name of borough, must be unique
        type: String,
        unique: true
    },
    vaccinated_pop:{  // Number of people that have been vaccinated 
        type: Number, // in this center
        default: 0
    },
    address:{
        type: String
    },
    // Coordinates for the center
    lat:{
        type: Number,
        required: true
    }, 
    lon:{
        type: Number,
        required: true
    }
})

const Center = mongoose.model('Center', centerSchema);

module.exports = Center;