export default function validateInfo(values){

    let errors = {};
    let successCtr = 0;

    // name
    if(!values.name.trim()){
        errors.name = "Se requiere el campo del nombre";
    } else if (values.name.length<3){
        errors.name = "El nombre debe tener al menos 3 letras";
    } else if(/[^a-zA-Z -]/.test(values.name)){
        errors.name = "Solo pon caractéres validos";
    } else {
        successCtr++;
    }

    // secondName
    if(!values.secondName.trim()){
        errors.secondName = "Se requiere el campo del apellido";
    } else if (values.secondName.length<3){
        errors.secondName = "Este campo debe tener al menos 3 letras";
    } else {
        successCtr++;
    }

    // email 
    if(!values.email){
        errors.email = "Se requiere la dirección de correo";
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)){
        errors.email = "La dirección es inválida";
    } else {
        successCtr++;
    }

    // phone
    if(!values.phone){
        errors.phone = "Se requiere un número de teléfono";
    } else if(values.phone.length<8){
        errors.phone = "Número no es válido";
    } else if(!/[0-9._%+-]/.test(values.phone)){
        errors.phone = "Número no es válido";
    } else {
        successCtr++;
    }

    // position
    if(!values.position){
        errors.position = "Por favor indique su ocupación";
    } else {
        successCtr++;
    }

    // password
    if(!values.password){
        errors.password = "Se requiere una contraseña";
    } else if(values.password.length<8){
        errors.password = "La contraseña es muy corta";
    } else if(!/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/.test(values.password)){
        errors.password = "La contraseña debe tener letras, números y al menos 1 caractér especial";
    } else {
        successCtr++;
    }

    // 2nd password
    if(!values.password2){
        errors.password2 = "Se requiere verificar la contraseña";
    } else if(values.password!==values.password2){
        errors.password2 = "Las contraseñas no coinciden";
    } else {
        successCtr++;
    }


    // curp  
    if(!values.curp){
        errors.curp = "Se requiere este campo";
    } else if(!/^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$/.test(values.curp)){
        errors.curp = "Verifica que hayas escrito bien tu curp";
    } else {
        successCtr++;
    }
    return {errors, successCtr};

}