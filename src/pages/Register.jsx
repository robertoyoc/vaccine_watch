
import React, {useState, useEffect} from 'react';
import {Form, Button} from 'react-bootstrap';
import useForm from '../useForm';
import validate from '../validateInfo';

export default function Register(){
    const {handleChange, handleSubmit, resetValues, values, errors} = useForm(validate);
    const currentYear = new Date().getFullYear();

    function handleClick (event){
        const regErrors = handleSubmit(event);
        
        if(JSON.stringify(regErrors) === '{}'){
            
            const parsedAge = currentYear - (1900 + parseInt(values.curp.substring(4, 6)));
            const parsedSex = values.curp.substring(10, 11);

            const userObject = {
                name: values.name,
                secondName: values.secondName,
                age: parsedAge,
                sex: parsedSex,
                curp : values.curp,
                email: values.email,
                phone: values.phone, 
                position: values.position,
                password: values.password,
            };
            
            console.log(userObject);

        } else {
            console.log("Registration form returned errors:", regErrors);
        }
    }

    return (
        <div>
            <h1>Registrarse</h1>
            <p>Por favor, llena todos los campos a continuación. </p>
            <div className="formDiv">

                <Form onSubmit={handleClick}>

                    {/* Name */}
                    <Form.Group controlId="validationName101">
                        <Form.Control 
                            type="text"
                            name="name" 
                            placeholder="Nombre" 
                            onChange={handleChange} 
                            value={values.name}
                            isInvalid = {!!errors.name}
                        />
                        {errors.name && <p>{errors.name}</p>}
                    </Form.Group>

                    {/* Second Name */}
                    <Form.Group controlId="exampleForm.ControlInput1">
                        <Form.Control 
                            type="text"
                            name="secondName" 
                            placeholder="Apellidos" 
                            onChange={handleChange} 
                            value={values.secondName} 
                            isInvalid = {!!errors.secondName}                           
                        />
                        {errors.secondName && <p>{errors.secondName}</p>}
                    </Form.Group>

                    {/* CURP */}
                    <Form.Group controlId="validationName101">
                        <Form.Control 
                            type="text"
                            name="curp" 
                            placeholder="CURP" 
                            onChange={handleChange} 
                            value={values.curp}
                            isInvalid = {!!errors.curp}
                        />
                        {errors.curp && <p>{errors.curp}</p>}
                    </Form.Group>

                    {/* Email */}
                    <Form.Group controlId="formGroupEmail">
                        <Form.Control 
                            type="email" 
                            name="email"
                            placeholder="Correo electrónico" 
                            onChange={handleChange}
                            value={values.email}
                            isInvalid = {!!errors.email}
                        />
                       {errors.email && <p>{errors.email}</p>} 
                    </Form.Group>

                    {/* Phone */}
                    <Form.Group controlId="formGroupPhone">
                        <Form.Control 
                            type="text" 
                            name="phone"
                            placeholder="Número telefónico" 
                            onChange={handleChange}
                            value={values.phone}
                            isInvalid = {!!errors.phone}
                        />
                       {errors.phone && <p>{errors.phone}</p>} 
                    </Form.Group>

                    {/* Position */}
                    <Form.Group controlId="formGroupPosition">
                        <Form.Control 
                            type="text" 
                            name="position"
                            placeholder="Ocupación profesional" 
                            onChange={handleChange}
                            value={values.position}
                            isInvalid = {!!errors.position}
                        />
                       {errors.position && <p>{errors.position}</p>} 
                    </Form.Group>

                    {/* Password */}
                    <Form.Group controlId="formGroupPassword">
                    <Form.Label>La contraseña debe tener 8 o más letras, mayusculas, números y signos especiales</Form.Label>
                        <Form.Control 
                            type="password" 
                            name="password"
                            placeholder="Contraseña" 
                            onChange={handleChange}
                            value={values.password}
                            isInvalid = {!!errors.password}
                        />
                       {errors.password && <p>{errors.password}</p>} 
                    </Form.Group>

                    {/* Confirm Password */}
                    <Form.Group controlId="formGroupPassword2">
                        <Form.Control 
                            type="password" 
                            name="password2"
                            placeholder="Confirmar contraseña" 
                            onChange={handleChange}
                            value={values.password2}
                            isInvalid = {!!errors.password2}
                        />
                       {errors.password2 && <p>{errors.password2}</p>} 
                    </Form.Group>
                
                    <Button variant="primary" type="submit">
                        Ingresar
                    </Button>
                </Form>
                
            </div>
            
        </div>
    )
}