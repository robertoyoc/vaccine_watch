import React, {useState, useEffect} from 'react';

const useForm = validate => {

    const emptyVal = {
        name: "",
        secondName:"",
        curp:"",
        email: "",
        phone:"", 
        occupation:"",
        password: "",
        password2: ""
    }

    const [values, setValues] = useState(emptyVal);

    const [errors, setErrors] = useState({})

    let errors2;

    const resetValues = () =>{
        setValues(emptyVal);
    } 

    const handleChange = event => {
        setValues({
            ...values,
            [event.target.name] : event.target.value
        })
    }
    
    const handleSubmit = event => {
        event.preventDefault();
        
        setErrors(validate(values)["errors"]);
        errors2 = validate(values)["errors"];
        
        return errors2;
        
    }

    return {handleChange, handleSubmit, resetValues, values, errors};
}

export default useForm;