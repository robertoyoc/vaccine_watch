import axios from 'axios';


export  const HOST = (process.env.NODE_ENV === 'production') ? 'https://api.vaccine-watch.robertoyoc.com' : `http://localhost:3000/`;

export default axios.create({
    baseURL: HOST
});