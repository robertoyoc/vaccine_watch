import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from './store'
import './index.scss';
import App from './App';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import reportWebVitals from './reportWebVitals';
import Amplify from 'aws-amplify';
import {awsconfig} from './aws-exports';
Amplify.configure(awsconfig);

const theme = createMuiTheme({
  typography:{
    fontFamily: 'montserrat-medium'
  },
  
  palette: {
    primary: {
      main: '#9D2449'
    }
  }
});

ReactDOM.render(
  <React.StrictMode>
    <head>
      <link rel="preconnect" href="https://fonts.gstatic.com"/>
      <link href="https://fonts.googleapis.com/css2?family=Montserrat&family=Patua+One&display=swap" rel="stylesheet"></link>
    </head>
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <App />
      </ThemeProvider>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
