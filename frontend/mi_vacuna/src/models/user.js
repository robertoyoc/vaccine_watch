export default class User {
    constructor(
        names,
        lastNames,
        residenceBorough,
        priority,
        percentile, 
        age, 
        sex, 
        curp, 
        email, 
        phone, 
        position,
        isVaccinated,
    ){
        this.names = names
        this.lastNames = lastNames
        this.residenceBorough = residenceBorough
        this.priority = priority
        this.percentile = percentile
        this.age = age
        this.sex = sex
        this.curp = curp
        this.email = email
        this.phone = phone
        this.position = position
        this.isVaccinated = isVaccinated
    }
}