import './App.scss';
import { BrowserRouter, Route } from 'react-router-dom';
import { ProtectedRoute } from './protectedRoute';
import NavBar from './components/navBar/navBar';
import Login from './components/login/login';
import Register from './components/register/register';
import Dashboard from './components/dashboard/dashboard';
import PriorityScreen from './components/priorityScreen/priorityScreen';
import Certificate from './components/certificate/certificate';
import TurnConfirmation from './components/turnConfirmation/turnConfirmation';
import CertificateRequest from './components/certificateRequest/certificateRequest';
import CertificateConfirmation from './components/certificateConfirmation/certificateConfirmation';
import ValidateComponent from './components/validate/validate'
function App() {

  return (
    <div>
      <BrowserRouter>
        <NavBar/>
        <Route 
          exact path='/'
          component={Login}
        />
        <Route 
          exact path='/register'
          render={(props) => (
              <Register/>
            )}
        />

        <ProtectedRoute
          exact path="/dashboard"
          component={Dashboard}
        />

        <ProtectedRoute 
          exact path='/priorityScreen'
          component={PriorityScreen}
        />

        <ProtectedRoute 
          exact path='/certificate'
          component={Certificate}
        />

        <ProtectedRoute
          exact path='/turnConfirmation'
          component={TurnConfirmation}
        />

        <ProtectedRoute
          exact path='/certificateRequest'
          component={CertificateRequest}
        />

        <ProtectedRoute
          exact path='/certificateConfirmation'
          component={CertificateConfirmation}
        />
        <ProtectedRoute
          exact path="/validate"
          component={ValidateComponent}
        />
      </BrowserRouter>
    </div>
  );
}

export default App;
