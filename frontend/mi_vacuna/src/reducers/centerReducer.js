import API from '../api'
import { setCenterInfo, setBorCenters, SET_CENTER_INFO, SET_BOR_CENTERS } from '../actions/centerActions'

const initState = {}

const centerReducer = (state=initState, action) => {
    switch(action.type){
        case SET_CENTER_INFO:
            console.log("Entered case SET_CENTER_INFO")
            return({
                ...state,
                currCenter: action.payload
            })
        case SET_BOR_CENTERS:
            return({
                ...state,
                currBorCenters: action.payload
            })
        default:
            return state
    }
}

export const getCenterInfo = (id_center) => async (dispatch, getState) => {
    return(API.get(`centers/getCenter/` + id_center).then((res)=>{
        return(dispatch(setCenterInfo(res.data.ctr)))
    }).catch((error)=>{
        console.log("error in API get: " + error)
        throw(error)
    }))
}

export const getBorCenters = (borough) => async (dispatch, getState) => {
    return(API.get(`centers/getCenters/` + borough)).then((res)=>{
        return(dispatch(setBorCenters(res.data.data)))
    })
}

export default centerReducer