import { AuthService } from '../services/auth/auth-service';
import { setLogin, setLogout, SET_LOG_IN, SET_LOG_OUT } from '../actions/authActions'

const initState = {logged: false}

const authReducer = (state=initState, action) => {
    switch(action.type){
        case SET_LOG_IN:
            return({
                ...state,
                logged: true,
                uuid: action.payload
            })
        case SET_LOG_OUT:
            return({
                ...state,
                logged: false,
                uuid: ""
            })
        default:
            return state
    }
}

export const login = (user, pass) => (dispatch, getState) => {
    console.log("logging in Cognito")

    let auth = new AuthService()

    return(auth.login(user, pass).then((res)=>{
        console.log("dispatched setlogin")
        return(dispatch(setLogin(res.userSub)))
    }).catch((error)=>{
        throw(error)
    }))
}

export const logout = () => (dispatch, getState) => {
    console.log("logging out of Cognito")

    let auth = new AuthService()

    return(auth.logout().then((res)=>{
        return(dispatch(setLogout()))
    }).catch((error)=>{
        throw(error)
    }))
}

export const isLogged = () => (dispatch, getState) => {
    console.log("Checking if logged")

    let auth = new AuthService()

    return(auth.getCurrentUuid().then((uuid)=>{
        console.log("user is logged")
        return(dispatch(setLogin(uuid)))
    }).catch((error)=>{
        console.log("user is not logged: " + error)
        return(dispatch(setLogout()))
    }))
}

export default authReducer