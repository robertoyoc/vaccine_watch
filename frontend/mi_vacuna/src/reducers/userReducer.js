import API from '../api';
import { HERE_REST_API_KEY } from '../keys';
import revgeocodeAPI from '../services/maps/hereRevgeoApi';
import { AuthService } from '../services/auth/auth-service';
import { setUserInfo, 
    createUserSuccess,
     setUserCoordsAddress,
      SET_USER_INFO, 
      getCertificated,
      CREATE_USER_SUCCESS, 
      SET_USER_COORDS_ADDRESS,
      GET_CERTIFICATE_USER
     } from '../actions/userActions';

/*const initState = {currentUser: new User(
    "Saúl Enrique",
    "Labra Cruz",
    "Álvaro Obregón",
    5,
    0.60,
    22,
    "Masculino",
    "ABCD12345",
    "a01234567@itesm.mx",
    "55123456",
    "Estudiante",
    false
)}*/

const initState = {
    currentUser:{
        isRequested: false,
        userApproved: false,
        isVaccinated: false,
        approved_list: [],
        name: "",
        secondName: "",
        residenceBorough: "",
        age: 0,
        sex: "",
        curp: "",
        email: "",
        phone: "",
        position: "",
        assignedPhase: "",
        priority: 0,
    }    
}

const userReducer = (state=initState, action) => {
    switch(action.type){
        case SET_USER_INFO:
            console.log("Entered case SET_USER_INFO")
            return({
                ...state,
                currentUser: action.payload
            })
        case CREATE_USER_SUCCESS:
            return({
                ...state,
                created_user_success: true
            })
        case SET_USER_COORDS_ADDRESS:
            return({
                ...state,
                coordsAddress: action.payload
            })
        case GET_CERTIFICATE_USER :
            return {
                ...state,
                certificated : action.payload
            }    
        default:
            return state
    }
}

export const fetchUserInfo = () => async (dispatch, getState) => {
    console.log("Entered to fetch")
    let userInfo
    let auth = new AuthService()

    return (auth.getCurrentUuid().then(uuid => {
        console.log("uuid retrieved from login: " + uuid)
        return(API.get(`users/getUser/` + uuid).then((res)=>{
            userInfo = res.data
            return(dispatch(setUserInfo(userInfo)))
        }).catch((error)=>{
            console.log("error in API get: " + error)
            throw(error)
        }))
    }).catch(error => {
        throw(error)
    }))
}

export const createUser = (userInfo) => async (dispatch, getState) => {
    console.log("Entered to create")

    console.log("Data to be sent: ")
    console.log(userInfo)

    let auth = new AuthService()
    let uuid
    
    return(auth.signUp(userInfo.email, userInfo.password).then((res) => {
        console.log(res.userSub)
        console.log("Created user in Cognito")

        uuid = res.userSub
        userInfo.uuid = res.userSub

        return(API.post(`users/createUser`, JSON.stringify(userInfo), {headers: {"Content-Type": "application/json"}}).then((res)=>{
            console.log(res)
            console.log(res.data)
            
            dispatch(createUserSuccess())
            console.log("created user in backend")

            console.log(uuid)
            let apiUrl = 'users/getuser/' + uuid
            
            return(API.get(apiUrl).then((res)=>{
                userInfo = res.data
                dispatch(setUserInfo(userInfo))
            }).catch((error)=>{
                console.log("error in API get: " + error)
            }))
        }).catch((error)=>{
            console.log("error in creating user: " + error)
            throw(error)
        }))
    }).catch((error)=>{
        console.log(error)
        throw(error)
    }))
}

export const confirmUser = (username, code) => async (dispatch, getState) => {
    console.log("Entered to confirm")

    console.log("Confirmation data:")
    console.log(username + " " + code)

    let auth = new AuthService()

    return(auth.confirmSignUp(username, code).then((res) => {
        console.log(res)
        console.log("Confirmed user")
    }).catch((error)=>{
        console.log(error)
        throw(error)
    }))
}

export const getCoordsAddress = (lat, long) => async (dispatch, getState) => {
    return(revgeocodeAPI.get('?at='+ lat +'%2C'+ long +'&lang=es-MX&apiKey=' + HERE_REST_API_KEY).then((res)=>{
        if(res.items.length > 0){
            return(dispatch(setUserCoordsAddress(res.items[0].title)))
        }else{
            return(dispatch(setUserCoordsAddress("")))
        }
    }))
}

export const userConfirm = (uuid) => async (dispatch, getState) => {
    return(API.patch(`users/userConfirmation/` + uuid).then(()=>{
        return(dispatch(fetchUserInfo()))
    }).catch((error)=>{
        console.log("error in API patch: " + error)
        throw(error)
    }))
}

export const confirmAttendance = (uuid, response) => async (dispatch, getState) => {
    return(API.patch(`users/confirmAttendance/` + uuid + `/` + response).catch((error)=>{
        console.log("Error in AttendanceConfirmation: " + error)
        throw(error)
    }))
}

    export const getCertificatedUser=(uuid) => async (dispatch) => {
        return(API.get(`certificate/generate/${uuid}`).then((res)=>{
            dispatch(getCertificated(res.config.url))
        }).catch((error)=>{
            console.log(error)
            throw(error)
        }))
    }

export default userReducer