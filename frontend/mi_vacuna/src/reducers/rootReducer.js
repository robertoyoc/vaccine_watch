import { combineReducers } from 'redux';
import authReducer from './authReducer';
import userReducer from './userReducer';
import boroughReducer from './boroughReducer';
import occupationReducer from './occupationReducer';
import centerReducer from './centerReducer';
import adminReducer from './adminReducer';

const rootReducer = combineReducers({
    auth: authReducer,
    user: userReducer,
    borough: boroughReducer,
    occupation: occupationReducer,
    center: centerReducer,
    admin: adminReducer
})

export default rootReducer;