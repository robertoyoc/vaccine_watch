import API from '../api'
import { setAvailableOccupations, SET_AVAILABLE_OCCUPATIONS } from '../actions/occupationActions'

const initState = {}

const occupationReducer = (state=initState, action) => {
    switch(action.type){
        case SET_AVAILABLE_OCCUPATIONS:
            console.log("Entered case SET_AVAILABLE_OCCUPATIONS")
            return({
                ...state,
                availableOccupations: action.payload
            })
        default:
            return state
    }
}

export const fetchAvailableOccupations = () => async (dispatch, getState) => {
    console.log("Entered to fetch")
    let availableOccupations

    return(API.get(`occs/getOccupations`).then((res)=>{
        availableOccupations = res.data
        return(dispatch(setAvailableOccupations(availableOccupations)))
    }).catch((error)=>{
        console.log("error in API get")
    }))
}

export default occupationReducer