import API from '../api'
import { 
    setRequestingUsers, 
    setRetrievedCurpUser, 
    setRetrievedCurpUserErrror ,
    SET_REQUESTING_USERS, 
    SET_RETRIEVED_CURP_USER , 
    SET_RETRIEVED_CURP_USER_ERROR,

} from '../actions/adminActions'


const initState = {}

const adminReducer = (state=initState, action) => {
    switch(action.type){
        case SET_REQUESTING_USERS:
            return({
                ...state,
                reqUsers: action.payload
            })
        case SET_RETRIEVED_CURP_USER:
            return{
                ...state,
                curpUser: action.payload,
                error : false
            }
        case SET_RETRIEVED_CURP_USER_ERROR :
            return{
                ...state,
                error : action.payload,
            }
        default:
            return state
    }
}

export const getRequestingUsers = (vaccCenter) => async (dispatch, getState) => {
    return(API.get(`admin/getRequestingUsers/${vaccCenter}` ).then((res)=>{
        return(dispatch(setRequestingUsers(res.data.data)))
    }).catch((error)=>{
        console.log("error in API get: " + error)
        if(error === "No se encontraron usuarios"){
            return(dispatch(setRequestingUsers([])))
        }else{
            throw(error)
        }
    }))
}

export const getUserByCurp = (curp) => async (dispatch, getState) => {
    return(API.get(`admin/getUserByCurp/${curp}`).then((res)=>{
        dispatch(setRetrievedCurpUserErrror(false))
        return(dispatch(setRetrievedCurpUser(res)))
        
    }).catch((error)=>{
        dispatch(setRetrievedCurpUserErrror(true))
        console.log(error)
        throw(error)
    }))
}

export const requestUser=(curp, doctor_id) => async () => {
    return(API.patch(`admin/requestUser/${curp}/${doctor_id}`).then((res)=>{
    }).catch((error)=>{
        console.log(error)
        throw(error)
    }))
}
export const setVaccinatedUser=(uuid, doctor_id) => async () => {
    return(API.patch(`admin/confirmVaccination/${uuid}/${doctor_id}`).then((res)=>{
        alert("vacunado")
    }).catch((error)=>{
        console.log(error)
        throw(error)
    }))
}

export const setValidateCertificate =(data) => async () => {
    return(API.post(`certificate/validate`, data).then((res)=>{
        return true;
    }).catch((error)=>{
        return false;
    }))
}
export default adminReducer