import API from '../api'
import { setAvailableBoroughs, SET_AVAILABLE_BOROUGHS } from '../actions/boroughActions'

const initState = {}

const boroughReducer = (state=initState, action) => {
    switch(action.type){
        case SET_AVAILABLE_BOROUGHS:
            console.log("Entered case SET_AVAILABLE_BOROUGHS")
            return({
                ...state,
                availableBoroughs: action.payload
            })
        default:
            return state
    }
}

export const fetchAvailableBoroughs = () => async (dispatch, getState) => {
    console.log("Entered to fetch")
    let availableBoroughs

    return(API.get(`bors/getBors`).then((res)=>{
        availableBoroughs = res.data
        return(dispatch(setAvailableBoroughs(availableBoroughs)))
    }).catch((error)=>{
        console.log("error in API get")
        throw(error)
    }))
}

export default boroughReducer