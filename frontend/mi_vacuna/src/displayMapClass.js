import * as React from 'react';
// @todo: @saullabra fix
import { HERE_API_KEY } from './keys';
import LinearProgress from '@material-ui/core/LinearProgress';

export class DisplayMapClass extends React.Component {
  mapRef = React.createRef();

  state = {
    // The map instance to use during cleanup
    map: null,
    loading: true
  };

  componentDidMount() {
    const H = window.H;
    const platform = new H.service.Platform({
        apikey: HERE_API_KEY
    });

    const defaultLayers = platform.createDefaultLayers();

    // Create an instance of the map
    const map = new H.Map(
      this.mapRef.current,
      defaultLayers.vector.normal.map,
      {
        // This map is centered over Europe
        center: { lat: this.props.lat, lng: this.props.lng },
        zoom: 16,
        pixelRatio: window.devicePixelRatio || 1
      }
    );

    var mapMarker = new H.map.Marker({lat:this.props.lat, lng:this.props.lng})
    map.addObject(mapMarker)

    this.setState({ map }, ()=>{
      this.setState({loading: false})
    });
  }

  componentWillUnmount() {
    // Cleanup after the map to avoid memory leaks when this component exits the page
    //this.state.map.dispose();
  }

  render() {
    let loadingDivStyle = {
      position: "absolute",
      height: this.props.height,
      width: this.props.width,
      display: "flex",
      justifyContent: "center",
      alignItems: 'center',
      zIndex: 4,
      backgroundColor: 'white'
    }

    let LinearProgressStyle = {
      width: '80%'
    }

    return (
      // Set a height on the map so it will display
      <div ref={this.mapRef} style={{ height: this.props.height, width: this.props.width}}>
        {this.state.loading ? <div style={loadingDivStyle}>
          <LinearProgress style={LinearProgressStyle}/>
        </div> : null}
      </div>
    );
  }
}