import { Auth } from 'aws-amplify';

export class AuthService {
    logged = false;

    async login(username, password) {
        try {
            const user = await Auth.signIn(username, password);
            return user
        } catch (error) {
            console.log(error)
            throw(error)
        }
    }

    async signUp(email, password) {
        try {
            const user = await Auth.signUp({
                username: email,
                password: password,
                attributes: {}
            })

            return user
        } catch (error) {
            console.log(error)
            throw(error)
        }
    }

    async newPassword(providerUser, pass) {
        try {
            const user = await Auth.completeNewPassword(providerUser, pass, null)
            return user
        } catch (error) {
            console.log(error)
        }
    }
    async logout() {
        try {
            await Auth.signOut();
        } catch (error) {
            console.log(error)
        }
    }
    async fetch() {
        return await Auth.currentAuthenticatedUser().then(u => {
            console.log('retrieved')
            return true
        }).catch(e => {
            console.log('not retrieved')
            return false
        })
    }

    async getCurrentUuid() {
        return await Auth.currentAuthenticatedUser().then(u => {
            return u.username
        }).catch(e => {
            throw(e)
        })
    }

    async confirmSignUp(username, code){
        try {
            await Auth.confirmSignUp(username, code);
            return true
        } catch (error) {
            console.log("Error confirming sign up", error)
            throw(new Error("Error confirming sign up: " + error))
        }
    }
}