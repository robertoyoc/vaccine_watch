import axios from 'axios';

const HOST = 'https://revgeocode.search.hereapi.com/v1/revgeocode';

export default axios.create({
    baseURL: HOST
});