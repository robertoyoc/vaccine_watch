import React from 'react';
import { Route, Redirect } from "react-router-dom";
import store from './store';

export const ProtectedRoute = ({ component: Component, ...rest}) => {
    return(
        <Route
            {...rest}
            render={props => {
                if(store.getState().auth.logged === true){
                    return <Component {...props} />
                }else{
                    return(
                        <Redirect
                            to={{
                                pathname: "/",
                                state: {
                                    from: props.location
                                }
                            }}
                        />
                    )
                }
            }}
        />
    )
}