export const SET_AVAILABLE_OCCUPATIONS = "SET_AVAILABLE_OCCUPATIONS"

export const setAvailableOccupations = (occupations) => {
    return({
        type: "SET_AVAILABLE_OCCUPATIONS",
        payload: occupations,
    })
}