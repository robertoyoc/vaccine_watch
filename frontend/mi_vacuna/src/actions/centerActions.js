export const SET_CENTER_INFO = "SET_CENTER_INFO"
export const SET_BOR_CENTERS = "SET_BOR_CENTERS"

export const setCenterInfo = (centerObject) => {
    return({
        type: "SET_CENTER_INFO",
        payload: centerObject,
    })
}

export const setBorCenters = (centers) => {
    return({
        type: "SET_BOR_CENTERS",
        payload: centers,
    })
}