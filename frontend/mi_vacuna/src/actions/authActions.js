export const SET_LOG_IN = "SET_LOG_IN"
export const SET_LOG_OUT = "SET_LOG_OUT"

export const setLogin = (uuid) => {
    return({
        type: SET_LOG_IN,
        payload: uuid
    })
}

export const setLogout = () => {
    return({
        type: SET_LOG_OUT,
    })
}