export const SET_AVAILABLE_BOROUGHS = "SET_AVAILABLE_BOROUGHS"

export const setAvailableBoroughs = (boroughs) => {
    return({
        type: "SET_AVAILABLE_BOROUGHS",
        payload: boroughs,
    })
}