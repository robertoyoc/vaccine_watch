export const SET_USER_INFO = "SET_USER_INFO";
export const CREATE_USER_SUCCESS = "CREATE_USER_SUCCESS";
export const SET_USER_COORDS_ADDRESS = "SET_USER_COORDS_ADDRESS";
export const GET_CERTIFICATE_USER ="GET_CERTIFICATE_USER";
export const setUserInfo = (userInfo) => {
    return({
        type: SET_USER_INFO,
        payload: userInfo,
    })
}

export const createUserSuccess = () => {
    return({
        type: CREATE_USER_SUCCESS,
    })
}

export const setUserCoordsAddress = (coordsAddress) => {
    return({
        type: SET_USER_COORDS_ADDRESS,
        payload: coordsAddress
    })
}

export const getCertificated =(certificate) => {
    return({
        type : GET_CERTIFICATE_USER,
        payload :certificate
    })
}