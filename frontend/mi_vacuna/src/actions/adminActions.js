export const SET_REQUESTING_USERS = "SET_REQUESTING_USERS"
export const SET_RETRIEVED_CURP_USER = "SET_RETRIEVED_CURP_USER"
export const SET_RETRIEVED_CURP_USER_ERROR = "SET_RETRIEVED_CURP_USER_ERROR"
export const setRequestingUsers = (requestingUsers) => {
    return({
        type: SET_REQUESTING_USERS,
        payload: requestingUsers,
    })
}

export const setRetrievedCurpUser = (userObject) => {
    return({
        type: SET_RETRIEVED_CURP_USER,
        payload: userObject
    })
}
export const setRetrievedCurpUserErrror=(error) => {
    return({
        type : SET_RETRIEVED_CURP_USER_ERROR,
        payload : error
    })
}
