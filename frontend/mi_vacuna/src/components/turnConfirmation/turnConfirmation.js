import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { fetchUserInfo, confirmAttendance } from '../../reducers/userReducer';
import { isLogged } from '../../reducers/authReducer';
import Footer from '../footer/footer';
import './turnConfirmation.scss';
import CustomCard from '../misc/customCard/customCard';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import BackButton from '../misc/backButton/backButton';
import { DisplayMapClass } from '../../displayMapClass';
import LoadingScreen from '../misc/loadingScreen/loadingScreen';
import AssignmentTurnedInIcon from '@material-ui/icons/AssignmentTurnedIn';
import AssignmentLateIcon from '@material-ui/icons/AssignmentLate';

class TurnConfirmation extends React.Component{
    state = {
        loading: true,
        date: "",
        location: "",
        schedule: "",
        vaccCenterLon: 0,
        vaccCenterLat: 0,
        turnConfirmed: false,
        hasDeclined: false,
    }

    componentDidMount(){
        this.props.dispatch(isLogged()).then(()=>{
            if(this.props.auth.logged){
                this.props.dispatch(fetchUserInfo()).then(()=>{
                    if(this.props.userInfo.currentUser.vaccDate){
                        this.setState({
                            date: this.props.userInfo.currentUser.vaccDate,
                            location: this.props.userInfo.currentUser.ctrAddress,
                            schedule: this.props.userInfo.currentUser.schedule,
                            turnConfirmed: this.props.userInfo.currentUser.hasAccepted,
                            vaccCenterLat: this.props.center.currCenter.lat,
                            vaccCenterLon: this.props.center.currCenter.lon,
                            loading: false
                        })
                    }
                })
            }
        })
    }

    handleConfirmation = () => {
        this.props.dispatch(isLogged()).then(()=>{
            this.props.dispatch(confirmAttendance(this.props.auth.uuid, 1)).then(()=>{
                this.props.dispatch(fetchUserInfo()).then(()=>{
                    this.setState({turnConfirmed: this.props.userInfo.currentUser.hasAccepted})
                })
            })
        })
    }

    handleDeclination = () => {
        this.props.dispatch(isLogged()).then(()=>{
            this.props.dispatch(confirmAttendance(this.props.auth.uuid, 0)).then(()=>{
                this.props.dispatch(fetchUserInfo()).then(()=>{
                    this.setState({
                        turnConfirmed: this.props.userInfo.currentUser.hasAccepted,
                        hasDeclined: true
                    })
                })
            })
        })
    }

    render(){
        return(
            this.state.loading ? <LoadingScreen/> :
            <div className="turn-confirmation-parent-div">
                <div className="turn-confirmation-content">
                    <h2 className="page-title">Tu Turno</h2>
                    <Grid container spacing={4} className="cards-container">
                        <Grid item md={6} sm={12} xs={12}>
                            <CustomCard className="card">
                                <div className="card-content">
                                    <h3>Detalles</h3>
                                    <ul className="list">
                                        <li className="bullet-none element">
                                            <p className="content-p-left"><strong>Fecha: </strong></p>
                                            <p className="content-p-right">{this.state.date}</p>
                                        </li>
                                        <li className="bullet-none element">
                                            <p className="content-p-left"><strong>Ubicación: </strong></p>
                                            <p className="content-p-right">{this.state.location}</p>
                                        </li>
                                        <li className="content-p bullet-none element">
                                            <p className="content-p-left"><strong>Horario: </strong></p>
                                            <p className="content-p-right">{this.state.schedule}</p>
                                        </li>
                                    </ul>
                                    {this.state.turnConfirmed ?
                                        <div className="confirmed-label">
                                            <h4>Turno Confirmado</h4>
                                            <AssignmentTurnedInIcon fontSize="large"/>
                                            <Button className="button" variant="contained" onClick={()=>{this.props.history.push("/priorityScreen")}}>Regresar</Button>
                                        </div>  
                                    : !this.state.turnConfirmed && !this.state.hasDeclined ?
                                            <div className="button-row">
                                                <Button className="button" variant="contained" color="primary" onClick={this.handleConfirmation}>Aceptar</Button>
                                                <Button className="button" variant="contained" onClick={this.handleDeclination}>Declinar</Button>
                                            </div>
                                        :
                                            <div className="confirmed-label">
                                                <h4>Turno Declinado. Espera a ser asignado nuevamente</h4>
                                                <AssignmentLateIcon fontSize="large"/>
                                                <Button className="button" variant="contained" onClick={()=>{this.props.history.push("/priorityScreen")}}>Regresar</Button>
                                            </div>   
                                    }
                                </div>
                            </CustomCard>
                        </Grid>
                        <Grid item md={6} sm={12} xs={12}>
                            <div className="card">
                                <DisplayMapClass height="100%" width="100%" lat={this.state.vaccCenterLat} lng={this.state.vaccCenterLon}/>
                            </div>
                        </Grid>
                    </Grid>
                </div>
                <BackButton onClick={()=>{this.props.history.push('/priorityScreen')}}>Regresar</BackButton>
                <Footer/>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return({
        userInfo: state.user,
        auth: state.auth,
        center: state.center
    })
}

export default connect(mapStateToProps)(withRouter(TurnConfirmation));