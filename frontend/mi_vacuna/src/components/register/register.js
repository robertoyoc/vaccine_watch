import React from 'react'
import RegisterCard from './registerCard'
import Footer from '../footer/footer'
import './register.scss'

class Register extends React.Component {
    render(){
        return(
            <div className="register-parent-div">
                <div className="title-container">
                    <h1 className="title underlined">Mi</h1>
                    <h1 className="title">Vacuna</h1>
                </div>
                <div className="content-div">
                    <RegisterCard/>
                </div>
                <Footer/>
            </div>
        )
    }
}

export default Register