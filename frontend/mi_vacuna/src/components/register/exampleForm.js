import React, { useState } from 'react';
import { useForm, Controller } from "react-hook-form";
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { FormHelperText } from '@material-ui/core';
import { ErrorMessage } from "@hookform/error-message";

export default function ExampleForm() {

    const { control, register, handleSubmit, watch, formState: { errors } } = useForm();

    const submitFunction = (data) => {
        console.log(data)
    }

    console.log("errors: ", errors)

    return(
        <div>
            <form onSubmit={handleSubmit(submitFunction)}>
                <Controller
                    name="field1"
                    control={control}
                    defaultValue=""
                    rules={{
                        required: "This field is required"
                    }}
                    render={({
                        field, fieldState
                    }) => {
                        return(<TextField
                            {...field}
                            required
                            className="field"
                            label="Field1"
                            variant="filled"
                        />)
                    }}
                />
                <ErrorMessage
                    errors={errors}
                    name="field1"
                    render={({ messages }) => {
                    console.log("messages", messages);
                    return messages
                        ? Object.entries(messages).map(([type, message]) => (
                            <p key={type}>{message}</p>
                        ))
                        : null;
                    }}
                />
                <Controller
                    name="Sexo"
                    rules={{
                        required: true
                    }}
                    render={({ field }) => (
                        <Select {...field}
                            label="Sexo"
                            required
                            error={false}
                            variant="filled"
                        >
                            <MenuItem value={"Masculino"}>Masculino</MenuItem>
                            <MenuItem value={"Femenino"}>Femenino</MenuItem>
                        </Select>
                    )}
                    control={control}
                />

                <Button
                    className="button"
                    variant="contained"
                    color="primary"
                    size="small"
                    //onClick={handleSubmit(submitFunction)}
                    type="submit"
                >
                    Confirmar
                </Button>
            </form>
        </div>
    )
}