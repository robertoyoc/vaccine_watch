import React from 'react';
import { connect } from 'react-redux'
import { Redirect, withRouter } from 'react-router-dom';

import CustomCard from '../misc/customCard/customCard';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { FormHelperText } from '@material-ui/core';
import './registerCard.scss';
import { fetchAvailableBoroughs } from '../../reducers/boroughReducer';
import { fetchAvailableOccupations } from '../../reducers/occupationReducer';
import { createUser, confirmUser, fetchUserInfo } from '../../reducers/userReducer';
import { login, isLogged } from '../../reducers/authReducer'
import { validateEmailFormat, validatePhoneLength, validatePasswordComposition } from './validationFunctions'
import { getUserByCurp } from '../../reducers/adminReducer';
import { getBorCenters } from '../../reducers/centerReducer';

class RegisterCard extends React.Component{
    state = {
        logged: false,
        curp: "",
        password: "",
        confirm_password: "",
        name: "",
        last_name: "",
        age: 0,
        sex: "",
        residenceBorough: '',
        occupation: '',
        email: "",
        phone: "",
        confirmation_code: "",
        userCoords: [],
        admin_id: '',
        admin_center: '',
        display_admin_id_field: false,
        incomplete_field_flags: {
            curp: false,
            password: false,
            confirm_password: false,
            name: false,
            last_name: false,
            age: false,
            sex: false,
            residenceBorough: false,
            occupation: false,
            email: false,
            phone: false,
            user_cords: false,
            confirmation_code: false,
        },
        incomplete_field_err_flag: false,
        incomplete_conf_code_flag: false,
        incomplete_field_err_txt: "",
        pwd_missmatch_err_flag: false,
        pwd_composition_err_flag: false,
        phone_composition_err_flag: false,
        email_error_flag: false,
        geolocation_err_flag: false,
        duplicate_curp_err_flag: false,
        submit_err_flag: false,
        duplicate_email_err_flag: false,
        no_centers_avail_err_flag: false,
        availableOccupations: [],
        availableBoroughs: [],
        availableAdminCenters: [],
        submitted_successfully: false,
        confirmed_successfully: false,
    }

    handleOccupationChange = (occupation_value) => {
        let show

        if(occupation_value === "Médico administrador de vacunas" || occupation_value === "Supervisor de vacunación"){
            show = true
        }else{
            show = false
        }

        this.setState({
            display_admin_id_field: show,
            occupation: occupation_value
        }) 
    }

    handleBoroughChange = (borValue) => {
        console.log("Received: " + borValue)
        this.setState({residenceBorough: borValue}, ()=>{
            this.props.dispatch(getBorCenters(borValue)).then(()=>{
                console.log("centers: " + this.props.center.currBorCenters)
                if(this.props.center.currBorCenters.length > 0){
                    this.setState({
                        availableAdminCenters: this.props.center.currBorCenters,
                        no_centers_avail_err_flag: false
                    })
                }else{
                    this.setState({
                        availableAdminCenters: [],
                        no_centers_avail_err_flag: true
                    })
                }
            }).catch(()=>{
                this.setState({
                    availableAdminCenters: [],
                    no_centers_avail_err_flag: true
                })
            })
        })
    }

    handleSubmit = () => {
        this.setState({
            incomplete_field_err_flag: false,
            incomplete_conf_code_flag: false,
            incomplete_field_err_txt: "",
            pwd_missmatch_err_flag: false,
            pwd_composition_err_flag: false,
            phone_composition_err_flag: false,
            email_error_flag: false,
            geolocation_err_flag: false,
            duplicate_curp_err_flag: false,
            duplicate_email_err_flag: false,
            confirm_user_err_flag: false,
            submit_err_flag: false,
        })
        
        if(
            this.state.curp === "" || 
            this.state.password === "" || 
            this.state.confirm_password === "" || 
            this.state.name === "" ||
            this.state.last_name === "" ||
            this.state.age === 0 ||
            this.state.sex === "" ||
            this.state.residenceBorough === "" ||
            this.state.occupation === "" ||
            this.state.email === "" ||
            this.state.phone === "" ||
            (this.state.admin_id === "" && this.state.display_admin_id_field) ||
            (this.state.admin_center === "" && this.state.display_admin_id_field)
        ){
            this.setState({
                incomplete_field_err_flag: true,
                incomplete_field_err_txt: "Este campo es necesario"
            })
        }else{
            let errorObj = {
                emailErr: false,
                phoneCompErr: false,
                pwdMissmatchErr: false,
                pwdCompositionErr: false
            }

            if(validateEmailFormat(this.state.email) === false){
                errorObj.emailErr = true
            }

            if(validatePhoneLength(this.state.phone) === false){
                errorObj.phoneCompErr = true
            }

            if(this.state.password !== this.state.confirm_password){
                errorObj.pwdMissmatchErr = true
            }

            if(validatePasswordComposition(this.state.password) === false){
                errorObj.pwdCompositionErr = true
            }

            this.setState({
                email_error_flag: errorObj.emailErr,
                phone_composition_err_flag: errorObj.phoneCompErr,
                pwd_missmatch_err_flag: errorObj.pwdMissmatchErr,
                pwd_composition_err_flag: errorObj.pwdCompositionErr
            }, ()=>{
                console.log(this.state)

                if(
                    !this.state.email_error_flag && 
                    !this.state.phone_composition_err_flag && 
                    !this.state.pwd_missmatch_err_flag &&
                    !this.state.pwd_composition_err_flag &&
                    !this.state.geolocation_err_flag
                ){
                    let newUserData = {
                        name: this.state.name,
                        secondName: this.state.last_name,
                        residenceBorough: this.state.residenceBorough,
                        age: Number(this.state.age),
                        sex: this.state.sex,
                        curp: this.state.curp,
                        email: this.state.email,
                        phone: this.state.phone,
                        position: this.state.occupation,
                        password: this.state.password ,
                        lat: this.state.userCoords[0],
                        lon: this.state.userCoords[1]
                    }

                    if(this.state.display_admin_id_field){
                        newUserData.doctor_id = this.state.admin_id
                        newUserData.currentCtr = this.state.admin_center
                    }
    
                    this.props.dispatch(getUserByCurp(newUserData.curp)).then(()=>{
                        if(this.props.admin.curpUser){
                            console.log(this.props.admin.curpUser)
                            this.setState({duplicate_curp_err_flag: true})
                            }else{
                            this.props.dispatch(createUser(newUserData)).then((res)=>{
                                console.log(res)
                                this.setState({submitted_successfully: true})
                            }).catch((error)=>{
                                this.setState({submit_err_flag: true, submitted_successfully: false}, ()=>{
                                    if(error.code === "UsernameExistsException"){
                                        this.setState({duplicate_email_err_flag: true})
                                    }

                                    if(error.error === "Se necesita asignar a este administrador a un centro específico."){
                                        this.setState({})
                                    }
                                })
                            })
                        }
                    }).catch((error)=>{
                    this.props.dispatch(createUser(newUserData)).then((res)=>{
                        console.log(res)
                        this.setState({submitted_successfully: true})
                    }).catch((error)=>{
                            this.setState({submit_err_flag: true, submitted_successfully: false}, ()=>{
                                if(error.code === "UsernameExistsException"){
                                    this.setState({duplicate_email_err_flag: true})
                                }
                            })
                        })
                    })     
                }
            })
        }
    }

    handleConfirmation = () => {
        if(this.state.confirmation_code === ''){
            this.setState({incomplete_conf_code_flag: true})
        }else{
            console.log(this.state)
            this.props.dispatch(confirmUser(this.state.email, this.state.confirmation_code)).then((res)=>{
                this.props.dispatch(login(this.state.email, this.state.password)).then(()=>{
                    this.setState({confirmed_successfully: true}, ()=>{
                        this.props.dispatch(fetchUserInfo()).catch((error)=>{
                            console.log(error)
                        })
                    })
                }).catch((error)=>{
                    console.log(error)
                })
            }).catch((error)=>{
                console.log(error)
                this.setState({confirm_user_err_flag: true, confirmed_successfully: false})
            })
        }
    }

    componentDidMount(){
        this.props.dispatch(isLogged()).then(()=>{
            if(!this.props.auth.logged){
                this.props.dispatch(fetchAvailableBoroughs()).then(()=>{
                    console.log(this.props.availableBoroughs)
                    if(this.props.availableBoroughs.data.length > 0){
                        this.setState({
                            availableBoroughs: this.props.availableBoroughs.data,
                        })
                    }
                }).then(()=>{
                    this.props.dispatch(fetchAvailableOccupations()).then(()=>{
                        console.log(this.props.availableOccupations)
        
                        if(this.props.availableOccupations.data.length > 0){
                            this.setState({
                                availableOccupations: this.props.availableOccupations.data,
                            })
                        }
                    })
                }).then(()=>{
                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition((coords) => {
                            this.setState({userCoords: [coords.coords.latitude, coords.coords.longitude]}, ()=>{
                                console.log(this.state.userCoords)
                            })
                        }, (error) => {
                            console.log(error)
                            this.setState({geolocation_err_flag: true})
                        });
                    } else { 
                        console.log("Geolocation is not supported by this browser.");
                    }
                })
            }
        })
    }

    render(){
        const redirect = this.props.auth.logged ? <Redirect to="/dashboard"/> : null

        return(
            <div className="register-card-parent-div">
                {redirect}
                <CustomCard className="paper-div">
                    {this.state.submitted_successfully && this.state.submit_err_flag === false && this.state.confirmed_successfully === false && this.state.confirm_user_err_flag === false && !this.state.duplicate_curp_err_flag ? <div className="register-success-card">
                        <h3>Confirma tu Usuario</h3>
                        <h4>Ingresa el código que recibiste en tu correo</h4>
                        <TextField
                            required
                            className="field"
                            label="Código"
                            variant="filled"
                            onChange={(e)=>this.setState({confirmation_code: e.target.value})}
                        />
                        <Button
                            className="button"
                            variant="contained"
                            color="primary"
                            size="small"
                            onClick={this.handleConfirmation}
                        >
                            Confirmar
                        </Button>
                        <p className="warning-label">Introduce el código para continuar</p>
                    </div> : null}
                    {this.state.submitted_successfully === false && this.state.duplicate_curp_err_flag === true ? <div className="register-success-card">
                        <h3>Error al crear usuario. CURP ya existente</h3>
                        <Button
                            variant="contained"
                            color="primary"
                            size="small"
                            onClick={() => {this.props.history.push("/")}}
                        >
                            Reintentar
                        </Button>
                    </div> : null}
                    {this.state.submitted_successfully === false && this.state.submit_err_flag === true ?  <div className="register-success-card">
                        <h3>Error al crear usuario {this.state.duplicate_email_err_flag ? "Este correo ya se ha registrado antes" : null}</h3>
                        <Button
                            variant="contained"
                            color="primary"
                            size="small"
                            onClick={() => {this.props.history.push("/")}}
                        >
                            Reintentar
                        </Button>
                    </div> : null}
                    {this.state.confirmed_successfully === false && this.state.confirm_user_err_flag === true ?  <div className="register-success-card">
                        <h3>El código de confirmación es incorrecto</h3>
                        <Button
                            variant="contained"
                            color="primary"
                            size="small"
                            onClick={() => {this.setState({confirmed_successfully: false, confirm_user_err_flag: false})}}
                        >
                            Reintentar
                        </Button>
                    </div> : null}
                    {this.state.submitted_successfully && this.state.submit_err_flag === false && this.state.confirmed_successfully === true && this.state.confirm_user_err_flag === false ?  <div className="register-success-card">
                        <h3>Usuario creado exitosamente</h3>
                        <Button
                            variant="contained"
                            color="primary"
                            size="small"
                            onClick={() => {this.props.history.push("/dashboard")}}
                        >
                            Siguiente
                        </Button>
                    </div> : null}
                    <div className="register-card-inner-div">
                        <h2>Registro</h2>
                        <form className="form-div">
                            <div className="column-div">
                                <TextField
                                    required
                                    helperText={this.state.incomplete_field_err_txt}
                                    error={this.state.incomplete_field_err_flag}
                                    className="field"
                                    label="Nombre(s)"
                                    variant="filled"
                                    onChange={(e)=>this.setState({name: e.target.value})}
                                />
                                <TextField
                                    required
                                    helperText={this.state.incomplete_field_err_txt}
                                    error={this.state.incomplete_field_err_flag}
                                    className="field"
                                    label="Apellido(s)"
                                    variant="filled"
                                    onChange={(e)=>this.setState({last_name: e.target.value})}
                                />
                                <TextField
                                    required
                                    helperText={this.state.incomplete_field_err_txt}
                                    error={this.state.incomplete_field_err_flag}
                                    className="field"
                                    label="CURP"
                                    variant="filled"
                                    onChange={(e)=>this.setState({curp: e.target.value})}
                                />
                                <TextField
                                    required
                                    helperText={this.state.incomplete_field_err_txt}
                                    error={this.state.incomplete_field_err_flag}
                                    className="field"
                                    label="correo electrónico"
                                    variant="filled"
                                    onChange={(e)=>this.setState({email: e.target.value})}
                                />
                                <TextField
                                    required
                                    helperText={this.state.incomplete_field_err_txt}
                                    error={this.state.incomplete_field_err_flag}
                                    className="field"
                                    label="Generar contraseña"
                                    type="Password"
                                    variant="filled"
                                    onChange={(e)=>this.setState({password: e.target.value})}
                                />
                                <TextField
                                    required
                                    helperText={this.state.incomplete_field_err_txt}
                                    error={this.state.incomplete_field_err_flag}
                                    className="field"
                                    label="Reescribir contraseña"
                                    type="Password"
                                    variant="filled"
                                    onChange={(e)=>this.setState({confirm_password: e.target.value})}
                                />
                            </div>
                            <div className="column-div">
                                <TextField
                                    required
                                    helperText={this.state.incomplete_field_err_txt}
                                    error={this.state.incomplete_field_err_flag}
                                    className="field"
                                    label="teléfono"
                                    variant="filled"
                                    onChange={(e)=>this.setState({phone: e.target.value})}
                                />
                                <TextField
                                    required
                                    helperText={this.state.incomplete_field_err_txt}
                                    error={this.state.incomplete_field_err_flag}
                                    className="field"
                                    label="Edad"
                                    variant="filled"
                                    type="number"
                                    onChange={(e)=>this.setState({age: e.target.value})}
                                />
                                <FormControl error={this.state.incomplete_field_err_flag}>
                                    <InputLabel id="sex-label" required error={this.state.incomplete_field_err_flag}>Sexo</InputLabel>
                                    <Select
                                        labelId="sex-label"
                                        required
                                        label="Sexo"
                                        error={this.state.incomplete_field_err_flag}
                                        className="field"
                                        value={this.state.sex}
                                        onChange={(e) => {this.setState({sex: e.target.value})}}
                                        variant="filled"
                                    >
                                        <MenuItem value={"Masculino"}>Masculino</MenuItem>
                                        <MenuItem value={"Femenino"}>Femenino</MenuItem>
                                    </Select>
                                    <FormHelperText>{this.state.incomplete_field_err_txt}</FormHelperText>
                                </FormControl>
                                <FormControl error={this.state.incomplete_field_err_flag}>
                                    <InputLabel required error={this.state.incomplete_field_err_flag}>Alcaldía</InputLabel>
                                    <Select
                                        required
                                        error={this.state.incomplete_field_err_flag}
                                        className="field"
                                        value={this.state.residenceBorough}
                                        onChange={(e)=>this.handleBoroughChange(e.target.value)}
                                        variant="filled"
                                    >
                                        {this.state.availableBoroughs.map((borough)=>{
                                            return(
                                                <MenuItem value={borough.name} key={borough.name}>{borough.name}</MenuItem>
                                            )
                                        })}
                                    </Select>
                                    <FormHelperText>{this.state.incomplete_field_err_txt}</FormHelperText>
                                </FormControl>
                                <FormControl error={this.state.incomplete_field_err_flag}>
                                    <InputLabel required error={this.state.incomplete_field_err_flag}>Ocupación</InputLabel>
                                    <Select
                                        required
                                        error={this.state.incomplete_field_err_flag}
                                        className="field"
                                        value={this.state.occupation}
                                        onChange={(e) => {this.handleOccupationChange(e.target.value)}}
                                        variant="filled"
                                    >
                                        {this.state.availableOccupations.map((occupation) => {
                                            return(
                                                <MenuItem value={occupation} key={occupation}>{occupation}</MenuItem>
                                            )
                                        })}
                                    </Select>
                                    <FormHelperText>{this.state.incomplete_field_err_txt}</FormHelperText>
                                </FormControl>
                                {this.state.display_admin_id_field ? <TextField
                                    required
                                    helperText={this.state.incomplete_field_err_txt}
                                    error={this.state.incomplete_field_err_flag}
                                    className="field"
                                    label="ID de administrador"
                                    variant="filled"
                                    onChange={(e)=>this.setState({admin_id: e.target.value})}
                                /> : null}
                                {this.state.display_admin_id_field ? <FormControl error={this.state.incomplete_field_err_flag}>
                                    <InputLabel required error={this.state.incomplete_field_err_flag}>Centro a administrar</InputLabel>
                                    <Select
                                        required
                                        error={this.state.incomplete_field_err_flag}
                                        className="field"
                                        value={this.state.admin_center}
                                        onChange={(e) => {this.setState({admin_center: e.target.value})}}
                                        variant="filled"
                                    >
                                        {this.state.availableAdminCenters.map((center)=>{
                                            return(
                                                <MenuItem value={center.id_center} key={center.id_center}>{center.name}</MenuItem>
                                            )
                                        })}
                                    </Select>
                                    <FormHelperText>{this.state.incomplete_field_err_txt}</FormHelperText>
                                </FormControl> : null}
                            </div>
                        </form>
                        <div className="error-box">
                            {this.state.geolocation_err_flag ? <div><p>Habilita la geolocalización para continuar</p></div> : null}
                            {this.state.email_error_flag ? <div><p>Introduce un correo electrónico válido</p></div> : null}
                            {this.state.phone_composition_err_flag ? <div><p>Introduce un número de teléfono de al menos 8 dígitos</p></div> : null}
                            {this.state.pwd_missmatch_err_flag ? <div><p>Las contraseñas introducidas no coinciden</p></div> : null}
                            {this.state.pwd_composition_err_flag ? 
                                <div>
                                    <p>Tu contraseña debe contener</p>
                                    <ul>
                                        <li>Mínimo 8 caracteres</li>
                                        <li>Un número</li>
                                        <li>Una letra mayúscula</li>
                                        <li>Una letra minúscula</li>
                                        <li>Un caracter especial</li>
                                    </ul>
                                </div> : null
                            }
                            {this.state.no_centers_avail_err_flag && (this.state.occupation === "Médico administrador de vacunas" || this.state.occupation === "Supervisor de vacunación") ? <div>
                                    <p>La alcaldía seleccionada no cuenta con centros de vacunación. Seleccionar una que cuente con ellos para continuar</p>
                                </div> : null
                            }
                        </div>
                        <Button
                            variant="contained"
                            color="primary"
                            className="next-btn"
                            size="small"
                            onClick={this.handleSubmit}
                            disabled={this.state.geolocation_err_flag}
                        >
                            Siguiente
                        </Button>
                    </div>
                </CustomCard>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return{
        availableBoroughs: state.borough.availableBoroughs,
        availableOccupations: state.occupation.availableOccupations,
        auth: state.auth,
        admin: state.admin,
        center: state.center
    }
}

export default connect(mapStateToProps)(withRouter(RegisterCard))