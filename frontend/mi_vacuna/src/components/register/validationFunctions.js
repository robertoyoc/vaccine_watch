export const validateEmailFormat = (email) => {
    var obj = /[^@]*@[^@]*/.exec(email)
    console.log(obj)

    if(obj){
        if(obj[0] === email){
            return true
        }else{
            return false
        }
    }else{
        return false
    }
}

export const validatePhoneLength = (phone) => {
    if(phone.length < 8){
        return false
    }else{
        return true
    }
}

export const validatePasswordComposition = (password) => {
    if(password.length < 8){
        return false
    }else{
        let obj = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[ !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~])(?=.{8,})/.exec(password)

        if(obj !== null){
            return true
        }else{
            return false
        }
    }
}