import React from 'react';
import './certificateRequest.scss';
import Footer from '../footer/footer';
import LoadingScreen from '../misc/loadingScreen/loadingScreen';
import CustomCard from '../misc/customCard/customCard';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import BackButton from '../misc/backButton/backButton'; 
import Checkbox from '@material-ui/core/Checkbox';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import {getUserByCurp ,requestUser} from '../../reducers/adminReducer'
class CertificateRequest extends React.Component{
    state = {
        loading: true,
        CurpField: "",
        retrievedInfo: false,
        names: "",
        lastNames: "",
        age: "",
        sex: "",
        occupation: "",
        turn: "",
        isRequested : false,
        asignedCenter: "",
        confirmationTextCheck: false,
        registered: false,
        invalidCurp: false,
        submitted: false,
        errorMsg: ""
    }

    confirmationText = "Bajo protesta de decir verdad, garantizo que esta persona ha recibido la dosis correspondiente de la vacuna para el COVID-19 y que sus datos personales son correctos"

    handleCheckChange = (e) => {
        this.setState({confirmationTextCheck: e.target.checked})
    }

    retrieveInfo = async ()  => {
        this.props.dispatch(getUserByCurp(this.state.curpField)).then(()=>{
            let user = this.props.data.curpUser
            this.setState({
                retrievedInfo: true,
                names : user.data.name,
                lastNames : user.data.secondName,
                age : user.data.age,
                sex : user.data.sex,
                occupation : user.data.position,
                turn : user.data.vaccDate + " " + user.data.schedule,
                asignedCenter : user.data.assignedCtr,
            })
        }).catch((error)=>{
            console.log(error.error)
        })        
    }

    submitCertificateRequest = () => {
        this.props.dispatch(requestUser(this.state.curpField, this.props.userInfo.currentUser.doctor_id)).then(()=>{
            this.setState({submitted: true})
        }).catch((error)=>{
            this.setState({errorMsg: error})
        })
    }
    
    componentDidMount(){
        this.setState({
            loading: false
        })
    }

    render(){
        let cardContent
        let requestInfoContent
        const { error} = this.props.data
        if(this.state.retrievedInfo && !this.state.invalidCurp && !this.state.registered ){
            requestInfoContent = <div className="information-section">
                <p>{this.state.errorMsg}</p>
                <ul className="list">
                    <li className="content-p bullet-none element">
                        <div><strong>Nombre(s): </strong></div>
                        <div>{this.state.names}</div>
                    </li>
                    <li className="content-p bullet-none element">
                        <div><strong>Apellido(s): </strong></div>
                        <div>{this.state.lastNames}</div>
                    </li>
                    <li className="content-p bullet-none element">
                        <div><strong>Edad: </strong></div>
                        <div>{this.state.age}</div>
                    </li>
                    <li className="content-p bullet-none element">
                        <div><strong>Sexo: </strong></div>
                        <div>{this.state.sex}</div>
                    </li>
                    <li className="content-p bullet-none element">
                        <div><strong>Ocupación: </strong></div>
                        <div>{this.state.occupation}</div>
                    </li>
                    <li className="content-p bullet-none element">
                        <div><strong>Turno: </strong></div>
                        <div>{this.state.turn}</div>
                    </li>
                    <li className="content-p bullet-none element">
                        <div><strong>Centro asignado: </strong></div>
                        <div>{this.state.asignedCenter}</div>
                    </li>
                </ul>
                <div className="confirmation-text-div">
                    <Checkbox
                        checked={this.state.confirmationTextCheck}
                        onChange={this.handleCheckChange}
                        inputProps={{ 'aria-label': 'primary checkbox' }}
                        color="primary"
                    />
                    <p className="certificate-emision-confirmation-check">{this.confirmationText}</p>
                </div>
                <div className="action-section">
                    <Button
                        className="action-section-button"
                        variant="contained"
                        color="primary"
                        size="small"
                        onClick={this.submitCertificateRequest}
                        disabled={!this.state.confirmationTextCheck}
                    >
                        Enviar
                    </Button>
                </div>
            </div>
        }else{
            if(this.state.retrievedInfo && this.state.invalidCurp){
                requestInfoContent = <h4>El CURP ingresado no es válido</h4>
            }else{
                if(this.state.retrievedInfo && this.state.registered){
                    requestInfoContent = <h4>El paciente seleccionado ya cuenta con certificado</h4>
                }
            }
        }

        if(this.state.submitted) {
            cardContent = <CustomCard className="search-card-div">
                <div className="card-content-div">
                    <h3 className="card-title">Solicitud registrada exitosamente</h3>
                    <div className="information-section">
                        <ul className="list">
                            <li className="content-p bullet-none element">
                                <div><strong>Nombre(s): </strong></div>
                                <div>{this.state.names}</div>
                            </li>
                            <li className="content-p bullet-none element">
                                <div><strong>Apellido(s): </strong></div>
                                <div>{this.state.lastNames}</div>
                            </li>
                            <li className="content-p bullet-none element">
                                <div><strong>Edad: </strong></div>
                                <div>{this.state.age}</div>
                            </li>
                            <li className="content-p bullet-none element">
                                <div><strong>Sexo: </strong></div>
                                <div>{this.state.sex}</div>
                            </li>
                            <li className="content-p bullet-none element">
                                <div><strong>Ocupación: </strong></div>
                                <div>{this.state.occupation}</div>
                            </li>
                            <li className="content-p bullet-none element">
                                <div><strong>Turno: </strong></div>
                                <div>{this.state.turn}</div>
                            </li>
                            <li className="content-p bullet-none element">
                                <div><strong>Centro asignado: </strong></div>
                                <div>{this.state.asignedCenter}</div>
                            </li>
                        </ul>
                    </div>
                    <div className="action-section">
                        <Button
                            className="action-section-button"
                            variant="contained"
                            size="small"
                            onClick={()=>{this.props.history.push("/dashboard")}}
                        >
                            Regresar
                        </Button>
                    </div>
                </div>
            </CustomCard>
        }else{
            cardContent = <CustomCard className="search-card-div">
            <div className="card-content-div">
                <h3 className="card-title">Búsqueda por CURP</h3>
                <div className="input-section">
                    <TextField
                        className="input-section-field"
                        label="CURP"
                        variant="filled"
                        onChange={(e)=>this.setState({curpField: e.target.value})}
                    />
                    <Button
                        className="input-section-button"
                        variant="contained"
                        size="small"
                        onClick={this.retrieveInfo}
                    >
                        Recuperar
                    </Button>
                </div>
                {requestInfoContent}
            </div>
        </CustomCard>
        }

        return(
            this.state.loading 
            ?
            <LoadingScreen/> 
            :
            (
                <div className="certificate-request-parent-div">
                <BackButton onClick={()=>{this.props.history.push("/dashboard")}}>Regresar</BackButton>
                <h2 className="section-title">Solicitud de certificado para paciente</h2>
                {cardContent}
                <Footer/>
            </div>
            )
        )
    }
}
const mapStateToProps = (state) => {
    return{
        data : state.admin,
        userInfo: state.user
    }
}


export default connect(mapStateToProps)(withRouter(CertificateRequest));