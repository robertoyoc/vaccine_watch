import React from "react";
import QrReader from "react-qr-reader";
import "./validate.styles.scss";
import { setValidateCertificate } from "../../reducers/adminReducer";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import Footer from "../footer/footer";
import Button from "@material-ui/core/Button";
import BackButton from "../misc/backButton/backButton";
import CustomCard from "../misc/customCard/customCard";

class ValidateComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      delay: 500,
      result: "Sin resultado",
      scanned: false,
    };

    this.handleScan = this.handleScan.bind(this);
  }
  handleScan(result) {
    const validateCertificate = async (data) => {
      return await this.props.dispatch(setValidateCertificate(data));
    };
    if (result) {
      const base64data = result.substring(40, result.length - 37).trim();
      const requestData = {
        data: base64data,
      };
      validateCertificate(requestData).then((isValid) => {
        console.log(isValid)
        const info = JSON.parse(atob(base64data));
        this.setState({
          result: `Este certificado pertenece a ${info.fullName}`,
          valid: isValid ? "Válido" : "Inválido",
          scanned: true,
        });
      });
    }
  }
  handleError(err) {
    console.error(err);
  }
  render() {
    const previewStyle = {
      height: 240,
      width: 320,
    };
    return (
      <div className={"container"}>
        {!this.state.scanned && (
          <div className="certificate-parent-div">
            <div className="certificate-div">
              <h2 className="page-title">Escanear</h2>
              <CustomCard className="certificate-card">
                <div className={"qr"}>
                  <QrReader
                    delay={this.state.delay}
                    style={previewStyle}
                    onError={this.handleError}
                    onScan={this.handleScan}
                  />
                </div>
              </CustomCard>
            </div>
            <BackButton
              onClick={() => {
                this.props.history.push("/dashboard");
              }}
            >
              Regresar
            </BackButton>
            <Footer />
          </div>
        )}
        {this.state.scanned && (
          <div className="certificate-parent-div">
            <div className="certificate-div">
              <h2 className="page-title">Resultado</h2>
              <CustomCard className="certificate-card">
                <div className="certificate-card-content">
                  <div className={"content"}>
                    <p>{this.state.result}</p>
                    <h5>Validez: {this.state.valid}</h5>
                  </div>
                  <Button
                    className="button"
                    variant="contained"
                    color="primary"
                    onClick={() => this.props.history.push("/certificate")}
                  >
                    Mi certificado
                  </Button>
                </div>
              </CustomCard>
            </div>
            <BackButton
              onClick={() => {
                this.props.history.push("/dashboard");
              }}
            >
              Regresar
            </BackButton>
            <Footer />
          </div>
        )}
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    userInfo: state.user,
    auth: state.auth,
  };
};
export default connect(mapStateToProps)(withRouter(ValidateComponent));
