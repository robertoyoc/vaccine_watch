import React from 'react';
import { connect } from 'react-redux';
import Paper from '@material-ui/core/Paper';
import CustomCard from '../misc/customCard/customCard';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

import './loginCard.scss';
import { Link, Redirect } from 'react-router-dom';

import { AuthService } from '../../services/auth/auth-service';
import { login, isLogged } from '../../reducers/authReducer';

class LoginCard extends React.Component {
    state = {
        form: {
            user: '',
            pass: '',
            newpass: ''
        },
        providerUser: null,
        error: false,
        newPassword: false,
        logged: false,
        error_incomplete_field: false,
        error_field_text: ""


    }

    constructor(props) {
        super(props)
        this.auth = new AuthService()
        this.auth.fetch().then(logged => {
            this.setState({
                ...this.state,
                logged: logged
            })

        })
    }
    async login(e) {
        const user = this.state.form.user, pass = this.state.form.pass;

        this.props.dispatch(login(user, pass)).then((u) => {
            if (u.challengeName && u.challengeName === "NEW_PASSWORD_REQUIRED") {
                this.setState({
                    providerUser: u,
                    newPassword: true
                })
            } 
            
            console.log('logged')
            console.log(u)
        }).catch(() => {
            this.setState({
                ...this.state,
                error: true
            })
        })
    }

    async newPassword() {
        const user = this.state.providerUser, pass = this.state.form.newpass;
        console.log(user)
        console.log(pass)
        this.auth.newPassword(user, pass).then((u) => {
            this.login(this.state.form.user, this.state.form.newpass)
        }).catch(() => {
            this.setState({
                ...this.state,
                error: true
            })
        })
    }
    handleInputChange = async e => {
        await this.setState({
            form: {
                ...this.state.form,
                [e.target.name]: e.target.value
            }

        });
    }

    componentDidMount(){
        this.props.dispatch(isLogged())
    }



    render() {
        let errorMesssage = (<p className="error-message">Error en el usuario/contraseña</p>)
        let error = this.state.error;
        const errorRender = (error) ? errorMesssage : null;

        
        let paper = (<CustomCard className="login-card-parent-div">
            <div className="login-paper-div">
                <h2>Iniciar Sesión</h2>
                <form className="form-div">
                    <TextField
                        name="user"
                        error={this.state.error_incomplete_field}
                        helperText={this.state.error_field_text}
                        label="Email"
                        variant="filled"
                        className="field"
                        onChange={this.handleInputChange}
                    />
                    <TextField
                        name="pass"
                        error={this.state.error_incomplete_field}
                        helperText={this.state.error_field_text}
                        label="Contraseña"
                        type="Password"
                        variant="filled"
                        className="field"
                        onChange={this.handleInputChange}

                    />


                </form>
                <Button
                    type="submit"
                    variant="contained"
                    color="primary"
                    className="login-btn"
                    size="small"
                    onClick={() => this.login()}
                >
                    Iniciar Sesión
                        </Button>

                <p>¿No tienes una cuenta? <Link to="register">Regístrate</Link></p>
            </div>
        </CustomCard>)


        let newPassword = (<Paper className="paper-div">
            <h2>Nueva contraseña</h2>
            <form className="form-div">
                <TextField
                    label="Email"
                    variant="filled"
                    className="field"
                    value={this.state.form.user}
                    onChange={this.handleInputChange}
                />
                <TextField
                    name="newpass"
                    label="nueva Contraseña"
                    type="Password"
                    variant="filled"
                    className="field"
                    value = {this.state.form.newpass}
                    onChange={this.handleInputChange}

                />

            </form>
            <Button
                    type="submit"
                    variant="contained"
                    color="secondary"
                    className="login-btn"
                    size="small"
                    onClick={() => this.newPassword()}
                >
                    Iniciar Sesión
                </Button>
        </Paper>)
        const main = this.state.newPassword ? newPassword : paper;
        let redirect = (this.props.auth.logged)? (<Redirect to="/dashboard" />): null;
        return (
            <div className="login-card-parent-div">
                {redirect}
                {main}
                {errorRender}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return({
        auth: state.auth
    })
}

export default connect(mapStateToProps)(LoginCard)