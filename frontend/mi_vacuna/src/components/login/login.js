import React from 'react';
import './login.scss';
import LoginCard from './loginCard';
import Footer from '../footer/footer';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom'

class Login extends React.Component {
    componentDidMount(){
        //this.props.dispatch(isLogged())
    }

    render(){
        return(
            <div className="login-parent-div">
                <div className="title-container">
                    <h1 className="title underlined">Mi</h1>
                    <h1 className="title">Vacuna</h1>
                </div>
                <div className="content-div">
                    <div className="login-card-div">
                        <LoginCard/>
                    </div>
                </div>
                <Footer/>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return({
        auth: state.auth
    })
}

export default connect(mapStateToProps)(withRouter(Login))