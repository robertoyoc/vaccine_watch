import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import Footer from '../../footer/footer';
import './loadingScreen.scss'

class LoadingScreen extends React.Component{
    render(){
        return(
            <div className="loading-screen-parent-div" {...this.props}>
                <CircularProgress color="primary" size={60}/>
                <Footer/>
            </div>
        )
    }
}

export default LoadingScreen;