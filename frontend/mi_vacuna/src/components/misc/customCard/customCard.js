import React from 'react';
import Paper from '@material-ui/core/Paper';
import './customCard.scss'

class CustomCard extends React.Component{

    render(){
        return(
            <div {...this.props}>
                <Paper className="custom-paper-div">
                    <div className="inner-div">
                        {this.props.children}
                    </div>
                </Paper>
            </div>
        )
    }
}

export default CustomCard