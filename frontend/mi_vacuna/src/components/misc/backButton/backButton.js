import React from 'react';
import Button from '@material-ui/core/Button';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';

class BackButton extends React.Component{
    render(){
        let style={
            position: "absolute",
            top: "10px",
            left: "10px"
        }

        return(
            <Button {...this.props} style={style}><ChevronLeftIcon/>{this.props.children}</Button>
        )
    }
}

export default BackButton;