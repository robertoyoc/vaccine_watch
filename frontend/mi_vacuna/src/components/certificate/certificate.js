import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import CustomCard from '../misc/customCard/customCard';
import Footer from '../footer/footer';
import LoadingScreen from '../misc/loadingScreen/loadingScreen';
import './certificate.scss';
import Button from '@material-ui/core/Button';
import BackButton from '../misc/backButton/backButton';
import Checkbox from '@material-ui/core/Checkbox';
import { isLogged } from '../../reducers/authReducer';
import { fetchUserInfo, userConfirm ,getCertificatedUser } from '../../reducers/userReducer';
import {HOST} from '../../api'
class Certificate extends React.Component {
    state = {
        loading: true,
        isVaccinated: false,
        userApproved: false,
        isRequested: false,
        date: "",
        location: "",
        schedule: "",
        checkedConfirmationText: false,
        certificated: ""
    }

    confirmationText = "Bajo protesta de decir verdad, garantizo que mi respuesta es correcta, y que mis datos personales son auténticos y están actualizados"

    handleCheckChange = (e) => {
        this.setState({checkedConfirmationText: e.target.checked})
    }

    handleConfirmation = () =>{
        console.log("Entered to confirm")
        this.props.dispatch(userConfirm(this.props.auth.uuid)).then(()=>{
            this.props.dispatch(fetchUserInfo()).then(()=>{
                this.setState({
                    isVaccinated: this.props.userInfo.currentUser.isVaccinated,
                    userApproved: this.props.userInfo.currentUser.userApproved,
                    isRequested: this.props.userInfo.currentUser.isRequested,
                })
            })
        }).catch((error)=>{
            console.log(error)
        })
    }

    handleDeclination = () => {
        this.props.history.push("/dashboard")
    }

    componentDidMount(){
        console.log(this.props.userInfo.currentUser)
        this.props.dispatch(isLogged()).then(()=>{
            if(this.props.auth.logged){
                this.props.dispatch(fetchUserInfo()).then(()=>{
                    this.setState({
                        isVaccinated: this.props.userInfo.currentUser.isVaccinated,
                        userApproved: this.props.userInfo.currentUser.userApproved,
                        isRequested: this.props.userInfo.currentUser.isRequested,
                        loading: false
                    }, ()=>{
                        if(this.props.userInfo.currentUser.vaccDate){
                            this.setState({
                                date: this.props.userInfo.currentUser.vaccDate,
                                location : this.props.userInfo.currentUser.ctrAddress,
                                schedule : this.props.userInfo.currentUser.schedule,

                            })
                        }
                    })
                })
            }
        })
        const load = async () => {
            await this.props.dispatch(getCertificatedUser(this.props.userInfo.currentUser.uuid));
            this.setState({certificated  : `${HOST}${this.props.userInfo.certificated}`})
        }
        load();
    }

    render(){
        const user = this.props.userInfo.currentUser
        let certificateCard = <div className="certificate-parent-div">
            <div className="certificate-div">
                <h2 className="page-title">Tu Certificado</h2>
                <CustomCard className="certificate-card">
                    <div className="certificate-card-content">
                        <h3 className="certificate-title-txt">{user.name}</h3>
                        <img src={this.state.certificated} alt="cargando certificado" height='350px' />
                        <p className="certificate-helper-txt">Presenta este certificado en pantalla o impreso como comprobante oficial de vacunación</p>
                        <Button className="button" variant="contained" color="primary" onClick={() =>  this.props.history.push("/validate")} >Validar un certificado</Button>

                    </div>
                </CustomCard>
            </div>
            <BackButton onClick={()=>{this.props.history.push('/dashboard')}}>Regresar</BackButton>
            <Footer/>
        </div>

        let certificateEmisionCard = <div className="certificate-parent-div">
            <div className="certificate-div">
                <h2 className="page-title">Proceso de emisión de certificados</h2>
                <CustomCard className="certificate-emision-card">
                    <div className="certificate-emision-card-content">
                        <h3 className="certificate-emision-title-txt">¿Se le aplicó a usted la vacuna del COVID-19 en el evento mostrado?</h3>
                        <ul className="list">
                            <li className="content-p bullet-none element">
                                <div><strong>Fecha: </strong></div>
                                <div>{this.state.date}</div>
                            </li>
                            <li className="content-p bullet-none element">
                                <div><strong>Ubicación: </strong></div>
                                <div>{this.state.location}</div>
                            </li>
                            <li className="content-p bullet-none element">
                                <div><strong>Horario: </strong></div>
                                <div>{this.state.schedule}</div>
                            </li>
                        </ul>
                        <div className="confirmation-text-div">
                            <Checkbox
                                checked={this.state.checkedConfirmationText}
                                onChange={this.handleCheckChange}
                                inputProps={{ 'aria-label': 'primary checkbox' }}
                                color="primary"
                            />
                            <p className="certificate-emision-confirmation-check">{this.confirmationText}</p>
                        </div>
                        <div className="button-row">
                            <Button className="button" variant="contained" color="primary" disabled={!this.state.checkedConfirmationText} onClick={this.handleConfirmation}>Aceptar</Button>
                            <Button className="button" variant="contained" onClick={this.handleDeclination}>Declinar</Button>
                        </div>
                    </div>
                </CustomCard>
            </div>
            <BackButton onClick={()=>{this.props.history.push('/dashboard')}}>Regresar</BackButton>
            <Footer/>
        </div>

        let emissionPendingCard = <div className="certificate-parent-div">
            <div className="certificate-div">
                <h2 className="page-title">Proceso de emisión de certificados</h2>
                <CustomCard className="certificate-emision-card">
                    <div className="certificate-emision-card-content">
                        <h3 className="certificate-emision-title-txt">Su certificado está siendo procesado con los siguientes datos</h3>
                        <ul className="list">
                            <li className="content-p bullet-none element">
                                <div><strong>Fecha: </strong></div>
                                <div>{this.state.date}</div>
                            </li>
                            <li className="content-p bullet-none element">
                                <div><strong>Ubicación: </strong></div>
                                <div>{this.state.location}</div>
                            </li>
                            <li className="content-p bullet-none element">
                                <div><strong>Horario: </strong></div>
                                <div>{this.state.schedule}</div>
                            </li>
                        </ul>                        
                    </div>
                </CustomCard>
            </div>
            <BackButton onClick={()=>{this.props.history.push('/dashboard')}}>Regresar</BackButton>
            <Footer/>
        </div>

        let waitForTurnCard = <div className="certificate-parent-div">
            <div className="certificate-div">
                <h2 className="page-title">Proceso de emisión de certificados</h2>
                <CustomCard className="certificate-emision-card">
                    <div className="certificate-emision-card-content">
                        <h3 className="certificate-emision-title-txt">Aún no cuentas con un certificado</h3>
                        <p className="certificate-emision-p">Espera a tu turno para ser vacunado. Tu médico solicitará el certificado para tu posterior confirmación en este mismo sitio</p>                       
                    </div>
                </CustomCard>
            </div>
            <BackButton onClick={()=>{this.props.history.push('/dashboard')}}>Regresar</BackButton>
            <Footer/>
        </div>

        let content
        if(this.state.isVaccinated){
            content = certificateCard
        }else{
            if(this.state.isRequested && !this.state.userApproved){
                content = certificateEmisionCard
            }else{
                if(this.state.isRequested && this.state.userApproved){
                    content = emissionPendingCard
                }else{
                    if(!this.state.isRequested && !this.state.userApproved && !this.state.isVaccinated){
                        content = waitForTurnCard
                    }
                }
            }
        }


        return(
            this.state.loading ? <LoadingScreen/> : content
        )
    }
}

const mapStateToProps = (state) => {
    return({
        userInfo: state.user,
        auth: state.auth
    })
}

export default connect(mapStateToProps)(withRouter(Certificate));