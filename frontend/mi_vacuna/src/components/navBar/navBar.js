import React from 'react';
import { connect } from 'react-redux';
import './navBar.scss';
import Button from '@material-ui/core/Button';
import { withRouter } from 'react-router-dom';
import { logout } from '../../reducers/authReducer';

class NavBar extends React.Component {    
    logout() {
        this.props.dispatch(logout()).then(()=>{
            this.props.history.push("/")
        }).catch((error)=>{
            console.log(error)
        })
    }

    componentDidMount(){
        console.log(this.props.location)
    }

    render(){
        const loggedOutNav = <div className="nav-parent-div"/>
        
        const loggedInNav = <div className="nav-parent-div">
            <div className="content-div">
                <div className="nav-left-content">
                    <div className="logo-nav-section">
                        <h1 className="nav-bar-logo logo-underlined">Mi</h1>
                        <h1 className="nav-bar-logo">Vacuna</h1>
                    </div>
                    <div className="nav-link-section">
                        <a href="/" className="nav-link">Inicio</a>
                    </div>
                </div>
                <div className="nav-bar-section right-section">
                    <Button variant="contained" color="primary" size="small" onClick={() => this.logout()}>Cerrar Sesión</Button>
                </div>
            </div>
        </div>
        
        return(
            <div>
                {this.props.auth.logged ? loggedInNav : loggedOutNav}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return({
        auth: state.auth
    })
}

export default connect(mapStateToProps)(withRouter(NavBar))