import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import Footer from '../footer/footer';
import { RadialBarChart, RadialBar, PieChart, Pie, Cell, ResponsiveContainer } from 'recharts';
import CustomCard from '../misc/customCard/customCard';
import './priorityScreen.scss';
import { DisplayMapClass } from '../../displayMapClass';
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/Info';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import Button from '@material-ui/core/Button';
import BackButton from '../misc/backButton/backButton';
import LoadingScreen from '../misc/loadingScreen/loadingScreen';
import { fetchUserInfo, getCoordsAddress } from '../../reducers/userReducer';
import { isLogged } from '../../reducers/authReducer';
import { getCenterInfo } from '../../reducers/centerReducer';


const priorityColors = [
    '#f54242',
    '#f59342',
    '#f5d742',
    '#ecf542',
    '#a4f542',
    '#54f542',
    '#42f57e',
    '#42f5b3',
    '#42f5e9',
    '#428af5'
]

const tmpCoords = [19.432483, -99.133317]
class PriorityScreen extends React.Component {  
    constructor(props){
        super(props)
        this.state = {
            loading: true,
            names: "",
            lastNames: "",
            residenceBorough: "",
            age: null,
            sex: "",
            email: "",
            phone: "",
            position: "",
            chartData: [
                {
                    name: '',
                    uv: 10,
                    pv: 2400,
                    fill: '#ffffff',
                }
            ],
            priority: 0,
            percentile: 0,
            showPercentileDialog: false,
            turnGaugeLabel: "-",
            turnGaugeActionEnabled: false,
            turnGaugeActionLabel: "Confirma tu turno",
            vaccCentAddressLabel: "-",
            vaccCenterLat: 0,
            vaccCenterLon: 0,
        }
    }

    componentDidMount() {
        console.log("mounted priority screen")
        this.props.dispatch(isLogged()).then(()=>{
            if(this.props.auth.logged){
                this.props.dispatch(fetchUserInfo()).then(()=>{
                    console.log(this.props.state)
                    console.log(this.props.userInfo.currentUser)
        
                    this.setState({
                        names: this.props.userInfo.currentUser.name,
                        lastNames: this.props.userInfo.currentUser.secondName,
                        curp: this.props.userInfo.currentUser.curp,
                        residenceBorough: this.props.userInfo.currentUser.residenceBorough,
                        age: this.props.userInfo.currentUser.age,
                        sex: this.props.userInfo.currentUser.sex,
                        email: this.props.userInfo.currentUser.email,
                        phone: this.props.userInfo.currentUser.phone,
                        position: this.props.userInfo.currentUser.position,
                        priority: this.props.userInfo.currentUser.priority,
                        percentile: this.props.userInfo.currentUser.q,
                    }, ()=>{
                        let turnGaugeLabel = this.state.turnGaugeLabel
                        let turnGaugeActionEnabled = this.state.turnGaugeActionEnabled
                        let turnGaugeActionLabel = this.state.turnGaugeActionLabel
                        let vaccCenterLat = this.state.vaccCenterLat
                        let vaccCenterLon = this.state.vaccCenterLon
                        let vaccCenterAddressLabel = this.state.vaccCenterAddressLabel

                        if(this.props.userInfo.currentUser.assignedCtr){
                            this.props.dispatch(getCenterInfo(this.props.userInfo.currentUser.assignedCtr)).then(()=>{
                                vaccCenterLon = this.props.center.currCenter.lon
                                vaccCenterLat = this.props.center.currCenter.lat
                                vaccCenterAddressLabel = this.props.center.currCenter.address

                                console.log(this.props.center.currCenter)

                                if(this.props.userInfo.currentUser.vaccDate){
                                    if(this.props.userInfo.currentUser.vaccDate !== "" || this.props.userInfo.currentUser.vaccDate !== null){      
                                        turnGaugeLabel = "Turno Asignado:\n" + this.props.userInfo.currentUser.vaccDate
                                        turnGaugeActionEnabled = true                        
                                        
                                        if(this.props.userInfo.currentUser.hasAccepted){
                                            turnGaugeActionLabel = "Consulta los detalles"
                                        }else{
                                            turnGaugeActionLabel = "Confirma tu asistencia"
                                        }
                                    }else{
                                        turnGaugeLabel = "Sin turno"
                                    }
                                }else{
                                    turnGaugeLabel = "Sin turno"
                                }
                                
                                this.setState({
                                    vaccCenterLat: vaccCenterLat,
                                    vaccCenterLon: vaccCenterLon,
                                    vaccCentAddressLabel: vaccCenterAddressLabel,
                                    turnGaugeLabel: turnGaugeLabel,
                                    turnGaugeActionEnabled: turnGaugeActionEnabled,
                                    turnGaugeActionLabel: turnGaugeActionLabel
                                }, ()=>{
                                    console.log(this.state)
                                    this.setState({loading:false})
                                })
                            })
                        }else{
                            vaccCenterAddressLabel = "Aún no cuentas con un centro de vacunación asignado"
                        }
                    })
                })
            }
        })
    }

    handleHoverEnter = (e) => {
        this.setState({showPercentileDialog: true})
    }

    handleHoverLeave = (e) => {
        this.setState({showPercentileDialog: false})
    }
    
    render(){
        let priorityChartData = [
            {
              name: '',
              uv: 10,
              fill: '#ffffff',
            },
            {
                name: 'Prioridad',
                uv: Math.abs(this.state.priority - 11),
                fill:  priorityColors[Math.abs(this.state.priority - 11)-1],
            }
        ];

        let percentileChartData = [
            { name: 'Group B', value: 100-(this.state.percentile*100), fill: '#FF9407' },
            { name: 'Group A', value: this.state.percentile*100, fill: '#eeeeee'},
          ];

        let loadingScreen = <LoadingScreen/>

        let screenContent = <div className="priority-screen-container">
            <h2 className="title-left">Mi Prioridad</h2>
            <div className="content-container">
                <div className="info-container">
                    <CustomCard className="details-container">
                        <h3 className="title-p">Perfil</h3>
                        <ul className="list">
                            <li className="content-p bullet-none element">
                                <div><strong>Nombre(s): </strong></div>
                                <div>{this.state.names}</div>
                            </li>
                            <li className="content-p bullet-none element">
                                <div><strong>Apellido(s): </strong></div>
                                <div>{this.state.lastNames}</div>
                            </li>
                            <li className="content-p bullet-none element">
                                <div><strong>Edad: </strong></div>
                                <div>{this.state.age}</div>
                            </li>
                            <li className="content-p bullet-none element">
                                <div><strong>Sexo: </strong></div>
                                <div>{this.state.sex}</div>
                            </li>
                            <li className="content-p bullet-none element">
                                <div><strong>Correo: </strong></div>
                                <div>{this.state.email}</div>
                            </li>
                            <li className="content-p bullet-none element">
                                <div><strong>Teléfono: </strong></div>
                                <div>{this.state.phone}</div>
                            </li>
                            <li className="content-p bullet-none element">
                                <div><strong>Ocupación: </strong></div>
                                <div>{this.state.position}</div>
                            </li>
                            <li className="content-p bullet-none element">
                                <div><strong>Alcaldía: </strong></div>
                                <div>{this.state.residenceBorough}</div>
                            </li>
                            <li className="content-p bullet-none element">
                                <div><strong>CURP: </strong></div>
                                <div>{this.state.curp}</div>
                            </li>
                        </ul>
                    </CustomCard>
                    <div className="charts-container">
                        <div className="module-container">
                            <h2>Prioridad</h2>
                            <div className="chart-container">
                                <div class="chart-center-label">
                                    <p>{this.state.priority}</p>
                                </div>
                                <ResponsiveContainer>
                                    <RadialBarChart cx="50%" cy="50%" innerRadius="20%" outerRadius="80%" barSize={60} data={priorityChartData} startAngle={180} endAngle={-180}>
                                        <RadialBar
                                            minAngle={15}
                                            background
                                            clockwise={true}
                                            dataKey="uv"
                                        />
                                    </RadialBarChart>
                                </ResponsiveContainer>
                            </div>
                        </div>
                        <div className="module-container">
                            <h2>Percentíl</h2>
                            <div className="chart-container">
                                <div class="chart-center-label">
                                    <p>{this.state.percentile*100 + "%"}</p>
                                </div>
                                <PieChart width={250} height={250}>
                                    <Pie
                                        data={percentileChartData}
                                        cx='50%'
                                        cy='50%'
                                        innerRadius={42}
                                        outerRadius={75}
                                        fill="#8884d8"
                                        paddingAngle={2}
                                        dataKey="value"
                                    >
                                        {percentileChartData.map((entry, index) => (
                                            <Cell key={`cell-${index}`} fill={entry.fill} />
                                        ))}
                                    </Pie>
                                </PieChart>
                                <div className="gauge-info-section">
                                    <IconButton className="info-button" onMouseEnter={this.handleHoverEnter} onMouseLeave={this.handleHoverLeave}>
                                        <InfoIcon className="icon-style"/>
                                    </IconButton>
                                    <CustomCard onMouseEnter={()=>{console.log("mouse entered dialog")}} className={`info-dialog ${this.state.showPercentileDialog ? "" : "info-dialog-hidden"}`}>
                                        <p className="p-dialog">Cuentas con más prioridad que el {this.state.percentile*100}% de las personas de tu localidad</p>
                                    </CustomCard>
                                </div>
                            </div>
                        </div>
                        <div className="module-container">
                            <h2>Turno</h2>
                            <div className="chart-container">
                                <div class="chart-center-label">
                                    <p className="p-label">{this.state.turnGaugeLabel}</p>
                                </div>
                                <span className="circle"/>
                                {this.state.turnGaugeActionEnabled ? <div className="gauge-action-section">
                                    <Button onClick={()=>{this.props.history.push('/turnConfirmation')}}>
                                        {this.state.turnGaugeActionLabel}
                                        <ChevronRightIcon/>
                                    </Button>
                                </div> : null}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="map-container">
                    <div className="map-info-div">
                        <CustomCard className="map-paper">
                            <p className="title-p">Centro de vacunación asignado</p>
                            <p className="content-p">{this.state.vaccCentAddressLabel}</p>
                        </CustomCard>
                    </div>
                    <DisplayMapClass height="100%" width="100%" lat={this.state.vaccCenterLat} lng={this.state.vaccCenterLon}/>
                </div>
            </div>
            <BackButton className="nav-button" onClick={()=>{this.props.history.push("/dashboard")}}>Regresar</BackButton>
            <Footer/>
        </div>

        return(
            this.state.loading ? loadingScreen : screenContent
        )
    }
}

const mapStateToProps = (state) => {
    return{
        dispatch: state.dispatch,
        userInfo: state.user,
        auth: state.auth,
        center: state.center,
        state:state
    }
}

export default connect(mapStateToProps)(withRouter(PriorityScreen));