import React from 'react';
import { connect } from 'react-redux';
import { fetchUserInfo } from '../../reducers/userReducer';
import './dashboard.scss';
import { withRouter } from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import CustomCard from '../misc/customCard/customCard';
import Footer from '../footer/footer';
import Button from '@material-ui/core/Button';
import CheckIcon from '@material-ui/icons/Check';
import { isLogged } from '../../reducers/authReducer';

const priorityColors = [
    '#f54242',
    '#f59342',
    '#f5d742',
    '#ecf542',
    '#a4f542',
    '#54f542',
    '#42f57e',
    '#42f5b3',
    '#42f5e9',
    '#428af5'
]
class Dashboard extends React.Component {
    state = {
        logged: true,
        priorityColor: '#eeeeee',
        names: "",
        lastNames: "",
        residenceBorough: "",
        age: 0,
        sex: "",
        email: "",
        phone: "",
        position: "",
        priority: 0,
        percentile: 0,
        vaccDate: "",
        certificateStatusLabel: "",
        priorityStatusLabel: "",
        isVaccinated: false,
        userApproved: false,
        isRequested: false,
        hasAccepted: false
    }

    componentDidMount(){
        this.props.dispatch(isLogged()).then(()=>{
            console.log(this.props.auth)
            console.log(this.props.userInfo.currentUser)
            if(this.props.auth.logged === true){
                this.props.dispatch(fetchUserInfo()).then(()=>{
                    console.log(this.props.userInfo.currentUser)

                    this.setState({
                        priorityColor: priorityColors[Math.abs(this.props.userInfo.currentUser.priority-11)-1],
                        names: this.props.userInfo.currentUser.name,
                        lastNames: this.props.userInfo.currentUser.secondName,
                        curp: this.props.userInfo.currentUser.curp,
                        residenceBorough: this.props.userInfo.currentUser.residenceBorough,
                        age: this.props.userInfo.currentUser.age,
                        sex: this.props.userInfo.currentUser.sex,
                        email: this.props.userInfo.currentUser.email,
                        phone: this.props.userInfo.currentUser.phone,
                        position: this.props.userInfo.currentUser.position,
                        priority: this.props.userInfo.currentUser.priority,
                        percentile: this.props.userInfo.currentUser.percentile,
                        isVaccinated: this.props.userInfo.currentUser.isVaccinated,
                        userApproved: this.props.userInfo.currentUser.userApproved,
                        isRequested: this.props.userInfo.currentUser.isRequested,
                        hasAccepted: this.props.userInfo.currentUser.hasAccepted
                    }, ()=>{
                        if(this.state.isVaccinated){
                            this.setState({certificateStatusLabel: "Vacunado. Certificado disponible"})
                        }else{
                            if(this.state.isRequested && !this.state.userApproved){
                                this.setState({certificateStatusLabel: "Certificado solicitado por médico. Confirma tu vacunación en esta sección para continuar con el proceso"})
                            }else{
                                if(this.state.isRequested && this.state.userApproved){
                                    this.setState({certificateStatusLabel: "Certificado en proceso de emisión"})
                                }else{
                                    if(!this.state.isRequested && !this.state.userApproved && !this.state.isVaccinated){
                                        this.setState({certificateStatusLabel: "No vacunado. Espera a ser vacunado y solicitud de tu médico"})
                                    }
                                }
                            }
                        }

                        if(this.props.userInfo.currentUser.vaccDate){
                            this.setState({vaccDate: this.props.userInfo.currentUser.vaccDate}, ()=>{
                                if(this.state.hasAccepted){
                                    this.setState({priorityStatusLabel: "Turno confirmado: " + this.state.vaccDate})
                                }else{
                                    if(!this.state.hasAccepted){
                                        this.setState({priorityStatusLabel: "Turno disponible, confírmalo en esta sección"})
                                    }
                                }
                            })
                        }else{
                            this.setState({priorityStatusLabel: "Sin turno, favor de esperar para asignación"})
                        }
                    })
                })
            }
        })
    }

    render() {
        const {position} = this.state
        console.log(position)
        return(
            <div className="dashboard-component-container">
                <div className="title-container">
                    <h1 className="title underlined">Mi</h1>
                    <h1 className="title">Vacuna</h1>
                </div>
                <div className="content-container">
                    <h2 className="section-title">Menú Principal</h2>
                        <Grid container spacing={4} className="cards-container">
                            <Grid item md={4} sm={6} xs={12}>
                                <CustomCard className="card">
                                    <div className="card-content">
                                        <h3 className="card-title">Revisa tus Datos</h3>
                                        <div className="summary-div">
                                            <ul className="list">
                                                <li className="content-p bullet-none element">
                                                    <div><strong>Nombre(s): </strong></div>
                                                    <div>{this.state.names}</div>
                                                </li>
                                                <li className="content-p bullet-none element">
                                                    <div><strong>Apellido(s): </strong></div>
                                                    <div>{this.state.lastNames}</div>
                                                </li>
                                                <li className="content-p bullet-none element">
                                                    <div><strong>Edad: </strong></div>
                                                    <div>{this.state.age}</div>
                                                </li>
                                                <li className="content-p bullet-none element">
                                                    <div><strong>Sexo: </strong></div>
                                                    <div>{this.state.sex}</div>
                                                </li>
                                                <li className="content-p bullet-none element">
                                                    <div><strong>Correo: </strong></div>
                                                    <div>{this.state.email}</div>
                                                </li>
                                                <li className="content-p bullet-none element">
                                                    <div><strong>Teléfono: </strong></div>
                                                    <div>{this.state.phone}</div>
                                                </li>
                                                <li className="content-p bullet-none element">
                                                    <div><strong>Ocupación: </strong></div>
                                                    <div>{this.state.position}</div>
                                                </li>
                                                <li className="content-p bullet-none element">
                                                    <div><strong>Alcaldía: </strong></div>
                                                    <div>{this.state.residenceBorough}</div>
                                                </li>
                                                <li className="content-p bullet-none element">
                                                    <div><strong>CURP: </strong></div>
                                                    <div>{this.state.curp}</div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </CustomCard>
                            </Grid>
                            <Grid item md={4} sm={6} xs={12}>
                                <CustomCard className="card">
                                    <div className="card-content">
                                        <h3 className="card-title">Prioridad</h3>
                                        <div className="circle-container">
                                            <span className="circle" style={{backgroundColor: this.state.priorityColor}}/>
                                            {this.state.hasAccepted ? <div className="checkmark-container"><CheckIcon className="checkmark" fontSize="large"/></div> : null}
                                        </div>
                                        <div>
                                            <div className="info-low">
                                                <p className="info-label">Tu nivel actual es:</p>
                                                <h4 className="info-label"><strong>{this.state.priority}</strong></h4>
                                                <p className="info-label"><strong>{this.state.priorityStatusLabel}</strong></p>
                                            </div>
                                            <div className="actions-container">
                                                <Button variant="contained" color="primary" onClick={()=>{this.props.history.push('/priorityScreen')}}>Más Información</Button>
                                            </div>
                                        </div>
                                    </div>
                                </CustomCard>
                            </Grid>
                            <Grid item md={4} sm={6} xs={12}>
                                <CustomCard className="card">
                                    <div className="card-content">
                                        <h3 className="card-title">Confirma tu vacunación y checa tu Certificado</h3>
                                        <div>
                                            <div className="info">
                                                <p>Actualmente tu status es:</p>
                                                <p><strong>{this.state.certificateStatusLabel}</strong></p>
                                            </div>
                                            <div className="actions-container">
                                                <Button variant="contained" color="primary" onClick={()=>{this.props.history.push('./certificate')}}>Más Información</Button>
                                            </div>
                                        </div>
                                    </div>
                                </CustomCard>
                            </Grid>
                            {
                            position.trim() === "Médico administrador de vacunas"
                            ?
                            (
                                <Grid item md={4} sm={6} xs={12}>
                                    <CustomCard className="card">
                                        <div className="card-content">
                                            <h3 className="card-title">Solicitar certificado para paciente</h3>
                                            <div>
                                                <div className="info">
                                                    <p>Solicita el registro de vacunación para tu paciente una vez aplicada la dosis</p>
                                                </div>
                                                <div className="actions-container">
                                                    <Button
                                                        variant="contained"
                                                        color="primary"
                                                        className="action-button"
                                                        onClick={()=>{this.props.history.push("/certificateRequest")}}
                                                    >
                                                        Solicitar
                                                    </Button>
                                                </div>
                                            </div>
                                        </div>
                                    </CustomCard>
                                </Grid>
                            )
                            :
                            null
                        }
                        {
                            position.trim() === "Supervisor de vacunación"
                            ?
                            (
                                <Grid item md={4} sm={6} xs={12}>
                                    <CustomCard className="card">
                                        <div className="card-content">
                                            <h3 className="card-title">Confirmar Vacunaciones</h3>
                                            <div>
                                                <div className="info">
                                                    <p>Confirma las vacunaciones pendientes de tus asignados</p>
                                                </div>
                                                <div className="actions-container">
                                                    <Button variant="contained" color="primary" className="action-button" onClick={()=>{this.props.history.push("/certificateConfirmation")}}>Solicitar</Button>
                                                </div>
                                            </div>
                                        </div>
                                    </CustomCard>
                                </Grid>
                            )
                            :
                            null
                        }
                        </Grid>                        
                    <br/>
                </div>
                <Footer/>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return{
        userInfo: state.user,
        auth: state.auth
    }
}

export default connect(mapStateToProps)(withRouter(Dashboard))