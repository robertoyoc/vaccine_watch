import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import './certificateConfirmation.scss';
import Footer from '../footer/footer';
import CustomCard from '../misc/customCard/customCard';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import IconButton from '@material-ui/core/IconButton';
import BackButton from '../misc/backButton/backButton';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import CancelIcon from '@material-ui/icons/Cancel';
import { getRequestingUsers,setVaccinatedUser } from '../../reducers/adminReducer';



class CertificateConfirmation extends React.Component{
    state={
        reqUsers: []
    }

    componentDidMount(){
        const loadData = async () => {
            await this.props.dispatch(getRequestingUsers(this.props.userInfo.currentUser.currentCtr))
            this.setState({reqUsers : this.props.admin.reqUsers})
        }
        loadData();
        
    }
    handleClick=  (id) => {
        console.log(id)
        this.props.dispatch(setVaccinatedUser(id, this.props.userInfo.currentUser.doctor_id)).then(()=>{
            this.props.dispatch(getRequestingUsers(this.props.userInfo.currentUser.currentCtr)).then(()=>{
                this.setState({reqUsers : this.props.admin.reqUsers})
            })
        })
    }

    render(){
        const {reqUsers} = this.state
        return(
            <div className="certificate-confirmation-parent-div">
                <BackButton onClick={()=>{this.props.history.push("/dashboard")}}>Regresar</BackButton>
                <h2 className="section-title">Confirmación de solicitudes</h2>
                <CustomCard className="card-div">
                    <h3 className="card-title">Pacientes</h3>
                    <TableContainer>
                        <Table>
                            <TableHead>
                            <TableRow>
                                <TableCell>Nombre(s)</TableCell>
                                <TableCell>Apellido(s)</TableCell>
                                <TableCell>CURP</TableCell>
                                <TableCell>Ubicación</TableCell>
                                <TableCell>Fecha</TableCell>
                                <TableCell>Horario</TableCell>
                                <TableCell></TableCell>
                            </TableRow>
                            </TableHead>
                            <TableBody>
                                {reqUsers.map((request)=>(
                                    <TableRow key={request.curp}>
                                        <TableCell component="th" scope="row">{request.name}</TableCell>
                                        <TableCell>{request.secondName}</TableCell>
                                        <TableCell>{request.curp}</TableCell>
                                        <TableCell>{request.residenceBorough}</TableCell>
                                        <TableCell>{request.vaccDate}</TableCell>
                                        <TableCell>{request.schedule}</TableCell>
                                        {!request.isVaccinated ? <TableCell align="right">
                                            <IconButton
                                            onClick={ () => this.handleClick(request.uuid)}
                                            >
                                                <CheckCircleIcon
                                                />
                                            </IconButton>
                                            <IconButton>
                                                <CancelIcon/>
                                            </IconButton>
                                        </TableCell>: null}
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </CustomCard>
                <Footer/>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return({
        admin: state.admin,
        userInfo: state.user
    })
}

export default connect(mapStateToProps)(withRouter(CertificateConfirmation))