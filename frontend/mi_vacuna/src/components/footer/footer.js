import React from 'react'
import './footer.scss'

class Footer extends React.Component {
    render(){
        return(
            <div className="footer-container">
                <div className="solid"/>
                <div className="pattern"/>
            </div>
        )
    }
}

export default Footer