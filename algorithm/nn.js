
const net = new brain.NeuralNetwork({
    hiddenLayers: [10, 5],
    activation: 'sigmoid'
});

const data = [
    {input:{ position: 0, age: 0.06451612903225806}, output: { 10:1}},
    {input:{ position: 0.375, age:0.04838709677419355}, output: { 9:1}},
    {input:{ position: 0.5, age:0.11290322580645161}, output: { 8:1}},
    {input:{ position: 0.375, age:0.24193548387096775}, output: { 7:1}}
  ];
  
function readInputs(stream, data) {
    for (let i = 0; i < data.length; i++) {
        stream.write(data[i]);
    }
    // let it know we've reached the end of the inputs
    stream.endInputs();
}

const trainingStream = new brain.TrainStream({
    neuralNetwork: net,
    /* *
     * Write training data to the stream. Called on each training iteration.
     */
    floodCallback: function () {
      readInputs(trainingStream, data);
    },
  
    /**
     * Called when the network is done training.
     */
    doneTrainingCallback: function (obj) {
      console.log(
        `trained in ${obj.iterations} iterations with error: ${obj.error}`
      );
  
      const result01 = net.run({ position: 0.375, age:0.3064516129032258}); // 7
      const result00 = net.run({ position: 0.125, age:0.1774193548387097}); // 8
      const result11 = net.run({ position: 0.5, age:0.016129032258064516}); // 10
      const result10 = net.run({ position: 0.5, age:0.04838709677419355}); // 9
  
      console.log(result01); 
      console.log(result00); 
      console.log(result11); 
      console.log(result10); 
    },
  });


// kick it off
readInputs(trainingStream, data);

const diagram = document.getElementById('diagram');
diagram.innerHTML = brain.utilities.toSVG(net);

/* const net = new brain.NeuralNetwork();
const xor = [
  { input: [0, 0], output: [0] },
  { input: [0, 1], output: [1] },
  { input: [1, 0], output: [1] },
  { input: [1, 1], output: [0] },
];

function readInputs(stream, data) {
  for (let i = 0; i < data.length; i++) {
    stream.write(data[i]);
  }
  // let it know we've reached the end of the inputs
  stream.endInputs();
}

const trainingStream = new brain.TrainStream({
  neuralNetwork: net,
  /**
   * Write training data to the stream. Called on each training iteration.
   */
  /* floodCallback: function () {
    readInputs(trainingStream, xor);
  },
 */
  /**
   * Called when the network is done training.
   */
  /* doneTrainingCallback: function (obj) {
    console.log(
      `trained in ${obj.iterations} iterations with error: ${obj.error}`
    );

    const result01 = net.run([0, 1]);
    const result00 = net.run([0, 0]);
    const result11 = net.run([1, 1]);
    const result10 = net.run([1, 0]);

    console.log('0 XOR 1: ', result01); // 0.987
    console.log('0 XOR 0: ', result00); // 0.058
    console.log('1 XOR 1: ', result11); // 0.087
    console.log('1 XOR 0: ', result10); // 0.934
  },
}); */

